


/*********************************************************************SERVER************************************************************************************************************************** */





const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const Pool = require("pg").Pool;
const csv = require('csv-parser')
const fs = require('fs');
var multer  = require('multer');
const fastcsv = require("fast-csv");
var morgan = require('morgan');
var nodemailer = require('nodemailer'); 
var format = require('pg-format');
const random = require('unique-random-string');
require('dotenv').config(
  { 
    path:'.env.production'
  }
);
var path = require('path');
var uniqid = require('uniqid'); 
/****mailid and password***********/
var transporter = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: '',
    pass: ''
  }
});
var upload = multer({ dest: 'uploads/' })
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    if (file.fieldname === 'jsfile') {
    
       cb(null,'public/'+process.env.PUBLIC_URL)
    
     
  }
  else if (file.fieldname === 'csvfile'){
    cb(null, 'uploads')
  }
  },
  filename: function (req, file, cb) {
    cb(null, uniqid.process()+file.originalname)
  }
})
 
var upload = multer({ storage: storage })
//postgres:postgres@localhost:5432/webgazesim
const pool = new Pool({
  // user: process.env.PGUSER,
  // host: process.env.PGHOST,
  // database:process.env.PGDATABASE,
  // password:process.env.PGPASSWORD ,
  // port: process.env.PGPORT
  user: "postgres",
  host: "localhost",
  database: "webgazesim",
  password: "postgres",
  port: 5432  
});

var accessLogStream = fs.createWriteStream(path.join(__dirname, 'access.log'), { flags: 'a' })
const app = express();

app.use(cors());
app.use(bodyParser.json()); 
app.use(morgan('combined', { stream: accessLogStream }))
const PORT = process.env.PORT ;

app.listen(PORT, () => {
  console.log(`Server is running ${PORT}.`);
});
/****************************************************************************Routes******************************************************************************************************** */
/*********************************************************************EADMIN*************************************************************************************************************************************************************************************/

//API to authenticate a user  and return value
app.post("/api/UserAuth0", (req, res) => {
  const { uemail , passwd } = req.body;
  pool.query(
    "SELECT userid, uname,isusernew,emailid,account_typeid,account_status FROM users WHERE  emailid = $1 AND paswd = crypt($2,paswd) "
    ,[uemail , passwd],
    (error, results) => {
       if(error|| !results.rows || !results.rows.length) {
           res.status(203).send("Invalid user or password"); 
        }
      else{ 
       if (results ) {
             res.status(200).json(results.rows);
         }
     }
        
   }
  );
});  


//API to create experiment into experiment and version table.
//returns  200 on success and 203 on error.
app.post("/api/createExpt",upload.fields([{name:'jsfile',maxCount:5}]),(req, res) => 
{
   var count=req.files['jsfile'].length;
  var jspath=[];
  for(i=0;i<count;i++)
  {
    filepath=req.files.jsfile[i].filename	;
    jspath.push(filepath);
    var jsp=JSON.stringify(jspath);
    jsp=jsp.replace("[","{");
    jsp=jsp.replace("]","}");
  }
  
  var dblvals = JSON.parse(req.body.dparams);
  var intvals = JSON.parse(req.body.iparams);
  var linkvals =JSON.parse(req.body.lparams);
  
  
  const{expname,exadminid,expdt,vno,nps}= req.body;
   
  pool.query( 
      "call addexpmnt($1,$2,$3,$4,$5,$6,$7,$8,$9)",
       [expname,exadminid,expdt,jsp,vno, nps,dblvals,intvals,linkvals],
       (error, results) => {
        if (error) {
          res.sendStatus(203);
        }
        else {
          res.sendStatus(200);    
         }
        }
        )
 });



//API to get the userid given emailid of user
app.post("/api/getUserid", (req, res) => {
  const { uemail } = req.body;
  pool.query(
    "SELECT userid FROM users WHERE  emailid = $1 AND account_status=true "
    ,[uemail],
    (error, results) => {
       if(error || !results.rows || !results.rows.length) {
           res.status(203).send("Invalid user account or EmailID"); 
        }
      else{ 
       if (results ) {
            res.status(200).json(results.rows);
         }
     }
    }
  );
});

//Eadmin: Viewdata to get all params for a given experiment.
app.post("/api/getallParams", (req, res) => {
  const {expid } = req.body;
  pool.query(
    "select doubleparams, intparams, links from versions where versionid =( select versionid from experiment where experimentid = $1)",
    [expid],
    (error, results) => {
      if (error) {
        res.status(203).send("error");
      }
      else {
       res.status(200).json(results.rows);
    } 
    });
});


//Eadmin: to get all user and their sessionid for all experiments of eadminid
app.get("/api/eadminexpusrssn/:id", (req, res) => {
  pool.query(
     "select experimentid, userid, sessionid, sdate from usersession where experimentid = $1",   
    [req.params.id],
    (error, results) => {
      if (error) {
        res.status(203).send("error");
      }
      else {
       res.status(200).json(results.rows);
    } 
    });
});


// get user name by userid used to send email mentioning their name
app.post("/api/usernamebyid", (req, res) => {
  const {uid } = req.body;
  pool.query(
    "select * from users  where userid =$1",
   [uid],
   (error, results) => {
     if (error) {
       res.status(203);
     }
     res.status(200).json(results.rows);
   }
  );
});


//API to retrieve all active experiments created by experiment admin from experiment table.
app.post("/api/getAllExpmnts", (req, res) => {
  const {eid } = req.body;
  pool.query(
    "SELECT  DISTINCT Experimentname, Experimentid  FROM experiment WHERE  ExptadminId = $1 AND exptactivestatus = true ",
    [eid],
    (error, results) => {
      if (error) {
        res.status(203);
      }
      else {
       res.status(200).json(results.rows);
    } 
    });
});


//API to get the versionid of given  expid  experiment admin from experiment table.
app.post("/api/getversionid", (req, res) => {
  const {eid,expid } = req.body;
  pool.query(
    "SELECT versionid FROM experiment WHERE  ExptadminId = $1 AND exptactivestatus = true  AND Experimentid =$2",
    [eid, expid],
    (error, results) => {
      if (error) {
        res.status(203).send("Error no versionid");
      }
      else {
       res.status(200).json(results.rows);
    } 
    });
});


//API to retrieve experiment active status  for specific experiment.
app.post("/api/getExpmtstats", (req, res) => {
  const {expid } = req.body;
  pool.query(
    "SELECT  exptactivestatus  FROM experiment WHERE  Experimentid = $1",
    [expid],
    (error, results) => {
      if (error) {
        res.status(203);
      }
      else {
       res.status(200).json(results.rows);
    } 
    });
});


//API to change experiment active status  for specific experiment.
app.put("/api/changeExptstats", (req, res) => {
  const {expid, value } = req.body;
  pool.query( 
    "UPDATE EXPERIMENT SET exptActivestatus = $2 WHERE  Experimentid = $1",
    [expid, value],
    (error, results) => {
      if (error) {
        res.status(203).send("error");
      }
      else {
       res.status(200).send("success");
    } 
    });
});


//API to retrieve all userdetails for given exptadminid used by ADD USERS.
app.post("/api/getusrfnf1", (req, res) => {
  const {expid } = req.body;
  pool.query(
    "SELECT * FROM adusrtab($1)",   [expid],
    (error, results) => {
      if (error) {
        res.status(203);
      }
      else {
       res.status(200).json(results.rows);
    } 
    });
});

//Eadmin: Viewdata --- to get simapp data 
//API to store params into  a csv file store it on client system   and returns success ie., 200 or failure  as 203
app.post("/api/getsimdata", (req, res) => {
  const {sid} = req.body;
  pool.query(
   "select dblval, intval,links,realtime from simapp  where simappid = (select simappid from usersession where sessionid = $1)",
  [sid],
  (error, results) => {
    if (error) {
     res.status(203); 
    }
    res.status(200).json(results.rows);
  }
 );
});

 
//eadmin
//API to update userexpt invitation details for given experiment used by ADD USERS menu.
app.post("/api/InviteuserExpt", (req, res) => {
  const {eid ,expid, verid, uid} = req.body;
  let query1 ='call InviteusertoExpt(\''+uid+'\',\' '+expid+'\', \''+verid+'\', \''+eid+'\')';
  pool.query(  query1, (error, results) => {
     if (error) {
     res.status(203);
   }
   else {
    res.status(200).send("success");
  }
 });
});



/*************************************************************************END OF EADMIN************************************************************************************************************** */
/***********************************************************************SUPER ADMIN****************************************************************************************************************** */
/*******************************Add Users********************************************************************************************/
 
app.post("/api/uploadCSV", upload.single('csvfile'),(req, res) => 
{
  let csvData = [];
  let stream = fs.createReadStream(req.file.path);
  let csvStream = fastcsv
    .parse()
    .on("data", function(data) {
      csvData.push(data);
    })
    .on("end", function() 
     {
      fs.unlinkSync(req.file.path);
      csvData.shift();
      if(csvData.length==0)
       {
             res.status(203).send('No data')
        }
      else
       {
         validData = true;
         for (index = 0; index < csvData.length; index++)
          {
              if (csvData[index].length != 4)  validData = false;
           }
         if (validData)
           {
              generatedQuery = 'insert into users(uname,paswd,account_typeid,emailid) values ';
              for (index = 0; index < csvData.length; index++)
               {     
                  generatedQuery = generatedQuery + '(\'' + csvData[index][0] + '\',crypt(\'' + csvData[index][1] + '\', gen_salt(\'bf\')),' + csvData[index][2] + ',\'' + csvData[index][3] + '\''+ ')';
                  if (index < csvData.length - 1)  generatedQuery = generatedQuery + ',';
               }
             pool.query(generatedQuery,[], (err, result) => 
              {
                 if (err) {
                        res.status(203).send(err.detail)
                     }           
                 else { 
                        res.status(200).send(result.rowCount + "  User added")                        
                      }
              });
            }
           else
            {
               res.status(203).send('Invalid CSV data.');
            }
    }    
});
stream.pipe(csvStream); 
});







/*******************************Manage Users*******************************************************************************************/
// All Users 
app.post("/api/allusers", (req, res) => {
  if(req.body.acctype==1)
  {
  pool.query(
   "SELECT * FROM users where account_status=True order by createddate  DESC ",
   [],
   (error, results) => {
     if (error) {
       res.sendStatus(500);
     }
     res.status(200).json(results.rows);
   }
  );
  }
  else
  res.sendStatus(401);
});


// Eadmin : Adduser send mail for adding users tp experiment
app.post("/api/smailadusr", (req, res) => {
  const {emailid ,ename, uname } = req.body;
  //var email ='umag1515@gmail.com';
  var mailOptions = {
    from: 'SIMAPP <>',
    to: emailid,
    subject: 'Invitation to run an experiment',
    text: 'Dear '+uname+','+ '\r\n'+ '     You are invited to run '+ename+ '. Please login and check your MYTASK.'
    };
  //console.log(mailOptions);  
  transporter.sendMail(mailOptions, function(error, results){
    if (error) {
      res.status(203);
    } else {
       res.sendStatus(200);
    }
  }); 
  res.sendStatus(200);
});


// sort users by date
app.post("/api/sortusers", (req, res) => {
  pool.query(
   "SELECT * FROM users where account_status=True order by createddate  ASC ",
   [],
   (error, results) => {
     if (error) {
       res.sendStatus(500);
     }
     res.status(200).json(results.rows);
   }
  );
});


// sort users by name
app.post("/api/sortalp", (req, res) => {
  if(req.body.acctype==1)
  {
  pool.query(
   "SELECT * FROM users where account_status=True order by uname ASC ",
   [],
   (error, results) => {
     if (error) {
       throw error;
     }
     res.status(200).json(results.rows);
   }
  );
  }
  else
  res.sendStatus(401);
});


// Delete Users
app.put("/api/deleteusers/:id", (req, res) => {
 pool.query(
  "update users set account_status=false where userid=$1",
  [req.params.id],
  (error, results) => {
    if (error) {
      res.sendStatus(500);
    }
    res.sendStatus(200);
  }
 );
});

// for passwd reset
app.post("/api/sendmail", (req, res) => {
  const {email} = req.body;
etoken=random(10, (err, randomString)=>{
  if(err)
      { return err }
      
      else{
          return randomString
      }
  })
  const validity = Date.now() + 180000;
  function mail()
  {
  var url= process.env.HOST +":" + process.env.RPORT
  var mailOptions = {
    from: 'SIMAPP',
    to:email ,
    subject: 'Click the link below to reset the password!', 
    text:url+'/ResetPassword?xwer='+email+'&gter='+etoken+'&vfge='+validity
   }
  res.status(200).send("Email sent");
  //console.log(mailOptions);
  transporter.sendMail(mailOptions, function(error, results){
    if (error) {
      res.sendStatus(500);
    } else {
       res.status(200).send("Email sent");
    }
  }); 
}
 pool.query(
    "select from users where emailid =$1",
    [email],
    (error, results) => {
      if (error) {
        res.sendStatus(500);
      }
      else
      {
     if((results.rowCount)==0)
     {
      res.status(203).send(" Email not found "); 
     }
     else
     {
      mail()
     }
      }
    }
   );
});


/*******************************Reset Password******************************************************************************************/
//Reset Password
app.post("/api/resetpwd", (req, res) => {
  const {emailid ,paswd } = req.body;
        pool.query (
          "update users set paswd=crypt($2, gen_salt('bf')) where emailid =$1",
          [emailid , paswd],
           (error, results) => {
            if (error) {
              res.sendStatus(500);
            }
           else{
            res.sendStatus(200);
          }
          })
       
   });

  

/***************************************************************************END OF SUPERADMIN*************************************************************************************************************** */




/**************************************************************************PARTICIPANTS****************************************************************************************************************/
/**************************************My Experiments*********************************************************************************** */
//Experiments of the user by userid
app.get("/api/experimentsbyuid/:id", (req, res) => {
  pool.query(
   "select distinct e.experimentname,ue.experimentid,ue.srvnvitedt,ue.completedtimes,ue.allowedtimes from userexperimentinvite ue join experiment e on ue.experimentid=e.experimentid where (ue.userid = $1)  and (ue.allowedtimes >= ue.completedtimes) and (e.exptexpirydt > CURRENT_DATE) and ( e.exptactivestatus = true)",
   [req.params.id],
   (error, results) => {
     if (error) {
        res.sendStatus(500);
     }
     res.status(200).json(results.rows);
   }
  );
});

/*************************************My Profile**************************************************************************************** */
//userinfo by userid
app.get("/api/usersbyid/:id", (req, res) => {
  pool.query(
    "select * from users  where userid =$1",
   [req.params.id],
   (error, results) => {
     if (error) {
       res.sendStatus(500);
     }
     res.status(200).json(results.rows);
   }
  );
});


//profileinfo by userid
app.get("/api/profilebyid/:id", (req, res) => {
  pool.query(
   "SELECT * FROM profileuser where profileid=$1 ",
   [req.params.id],
   (error, results) => {
     if (error) {
       res.sendStatus(500);
     }
     res.status(200).json(results.rows);
   }
  );
});


// update profile
app.post("/api/updateprofile/:id", (req, res) => {
  pool.query(
   "update profileuser set gender=$2,agegroup=$3,industry=$4,highestdegree=$5,profile_type=$6 where profileid=$1 ",
   [req.params.id,req.body.gender,req.body.agegroup,req.body.industry,req.body.highestdegree,req.body.profile_type],
   (error, results) => {
     if (error) {
      res.sendStatus(500);
     }
     res.sendStatus(200);
   }
  );
});



// Add Profile
app.post("/api/profileuser/:id", (req, res) => {
  pool.query(
    "call Addprofile ($1,$2,$3,$4,$5,$6)",
   [req.params.id,req.body.gender,req.body.agegroup,req.body.industry,req.body.highestdegree,req.body.profile_type],
   (error, results) => {
     if (error) {
      res.sendStatus(500);
     }
     else{
       res.sendStatus(200);
     }
   }
  );
});


/*****************************************My TASK*************************************************************************************** */
// Experiment info by Experiment ID
app.get("/api/JSFile/:id", (req, res) => {
  pool.query(
    "SELECT * FROM experiment  where experimentid=$1",
    [req.params.id],
   (error, results) => {
     if (error) {
        res.sendStatus(500);
     }
     if(results){
      res.status(200).json(results.rows);
    }
   }
  );
}
);




// Runs experiment from participant --- Stores experiment data. 
app.post("/api/Senddata/:uids/:eids/:comple", upload.none(),(req, res) => {
  var userid =req.params.uids;
  var expid =req.params.eids;
  var complete;
  if (req.params.comple > 0) {complete = true;}
  else {complete = false;}
  let query1 ='call Updtuserssn(\''+userid+'\',\''+expid+'\', '+complete+')';
  pool.query(  query1, (error, results) => {
  if (error) {
        res.status(203);
  }
  else {
   ssnid = JSON.parse(JSON.stringify(results.rows[0]));
   var ssn1 =ssnid['ssnid'];
   var simid; 
   pool.query(
      "SELECT Simappid FROM Usersession WHERE userid = $1 AND sessionid =$2 AND experimentid =$3 ",
      [userid,ssn1,expid],
       (error, results) => {
        if (error) {
          console.log(error);
         }
       else {
         simid = JSON.parse(JSON.stringify(results.rows[0]));
         var simid1 = simid['simappid'];
         var rtimer;
         var dblvals = JSON.parse(req.body.dblvalue);
         var total = Object.keys(dblvals).length; 
         var intvals = JSON.parse(req.body.intvalue);
         var linkvals = JSON.parse(req.body.linkvalue);
         var rtime =JSON.parse(req.body.rtime);
         for(var i =0; i<total ; i++)
         { 
          rtimer =JSON.parse(rtime[i]);
          dblvals[i]=JSON.stringify(dblvals[i]).replace("[","{");
          dblvals[i]=JSON.stringify(dblvals[i]).replace("]","}");
          dblval = JSON.parse(dblvals[i]);
          intvals[i]=JSON.stringify(intvals[i]).replace("[","{");
          intvals[i]=JSON.stringify(intvals[i]).replace("]","}");
          intval = JSON.parse(intvals[i]);
          linkvals[i]=JSON.stringify(linkvals[i]).replace("[","{");
          linkvals[i]=JSON.stringify(linkvals[i]).replace("]","}");
          linkval = JSON.parse(linkvals[i]);
         
         pool.query(
             "INSERT INTO Simapp(simappid,dblval,intval,links,realtime) VALUES($1, $2, $3, $4, $5)" ,
             [simid1, dblval, intval, linkval ,rtimer],
             (error, results) => {
               if (error) {
                 res.status(203);
               }
             else {
                res.status(200);
             } 
         });
       } // for loop
       }
     }); // getting simappid
    }
   }); //adding session id
}); 



/*****************************************Terms**********************************************************************************************/
// Term info update by userid
app.put("/api/termsagreed/:id", (req, res) => {
  pool.query(
    "update users set isusernew=false,termsagreed=true where userid =$1",
   [req.params.id],
   (error, results) => {
     if (error) {
      res.sendStatus(500);
     }
     else{
       res.sendStatus(200);
     }
   }
  );
});


/*********************************************************************END OF PARTICIPANTS********************************************************************************************************************************************************************** */




 







