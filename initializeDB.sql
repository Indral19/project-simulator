
SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

CREATE EXTENSION IF NOT EXISTS pgcrypto WITH SCHEMA public;


COMMENT ON EXTENSION pgcrypto IS 'cryptographic functions';


CREATE TABLE public.experiment (
    experimentname character varying(200) NOT NULL,
    versionid bigint,
    srvcreatedate date DEFAULT CURRENT_DATE,
    srvcreatetime time without time zone DEFAULT CURRENT_TIME,
    exptexpirydt date NOT NULL,
    exptactivestatus boolean DEFAULT true,
    exptadminid bigint,
    exptjslink text[],
    initcode text,
    simcode text,
    termcode text,
    gblvardecl text,
    experimentid bigint NOT NULL
);


ALTER TABLE public.experiment OWNER TO postgres;

ALTER TABLE public.experiment ALTER COLUMN experimentid ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.experiment_experimentid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


CREATE TABLE public.profileuser (
    gender character varying(20) NOT NULL,
    agegroup integer NOT NULL,
    industry character varying(200) NOT NULL,
    highestdegree integer NOT NULL,
    profile_type character varying(50) NOT NULL,
    profileno bigint NOT NULL,
    profileid bigint
);


ALTER TABLE public.profileuser OWNER TO postgres;

ALTER TABLE public.profileuser ALTER COLUMN profileno ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.profileuser_profileno_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


CREATE TABLE public.simapp (
    simappid bigint,
    dblval double precision[],
    intval integer[],
    links text[],
    realtime bigint,
    said bigint NOT NULL
);


ALTER TABLE public.simapp OWNER TO postgres;


ALTER TABLE public.simapp ALTER COLUMN said ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.simapp_said_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


CREATE TABLE public.userexperimentinvite (
    userid bigint,
    experimentid bigint,
    versionid bigint,
    exptadminid bigint,
    srvnvitedt date DEFAULT CURRENT_DATE,
    srvinvitetm time without time zone DEFAULT CURRENT_TIME,
    allowedtimes integer DEFAULT 1,
    completedtimes integer DEFAULT 0,
    inviteid bigint NOT NULL
);


ALTER TABLE public.userexperimentinvite OWNER TO postgres;


ALTER TABLE public.userexperimentinvite ALTER COLUMN inviteid ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.userexperimentinvite_inviteid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


CREATE TABLE public.users (
    userid bigint NOT NULL,
    uname character varying(50) NOT NULL,
    userfname character varying(100),
    userlname character varying(100),
    createddate date DEFAULT CURRENT_DATE,
    createdtime time without time zone DEFAULT CURRENT_TIME,
    ucity character varying(50),
    ucountry character varying(50),
    account_typeid integer,
    isusernew boolean DEFAULT true,
    termsagreed boolean DEFAULT false,
    account_status boolean DEFAULT true,
    emailid character varying(70) NOT NULL,
    paswd character varying(300) NOT NULL,
    profileid bigint NOT NULL
);


ALTER TABLE public.users OWNER TO postgres;


ALTER TABLE public.users ALTER COLUMN profileid ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.users_profileid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);



ALTER TABLE public.users ALTER COLUMN userid ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.users_userid_seq1
    START WITH 1000
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);



CREATE TABLE public.usersession (
    userid bigint NOT NULL,
    experimentid bigint,
    serverdate date DEFAULT CURRENT_DATE,
    servertime time without time zone DEFAULT CURRENT_TIME,
    sdate date,
    stime time without time zone,
    complete boolean DEFAULT false,
    sessionid bigint NOT NULL,
    simappid bigint NOT NULL,
    wgid bigint NOT NULL
);


ALTER TABLE public.usersession OWNER TO postgres;


ALTER TABLE public.usersession ALTER COLUMN sessionid ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.usersession_sessionid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);



ALTER TABLE public.usersession ALTER COLUMN simappid ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.usersession_simappid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);



ALTER TABLE public.usersession ALTER COLUMN wgid ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.usersession_wgid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);



CREATE TABLE public.versions (
    versionno bigint NOT NULL,
    noofparams integer NOT NULL,
    doubleparams text[],
    intparams text[],
    links text[],
    versionid bigint NOT NULL
);


ALTER TABLE public.versions OWNER TO postgres;


ALTER TABLE public.versions ALTER COLUMN versionid ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.versions_versionid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);



CREATE TABLE public.webgazer (
    wgid bigint NOT NULL,
    xvalue real,
    yvalue real,
    wgtime bigint,
    wgno bigint NOT NULL
);


ALTER TABLE public.webgazer OWNER TO postgres;


ALTER TABLE public.webgazer ALTER COLUMN wgno ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.webgazer_wgno_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);



ALTER TABLE ONLY public.users
    ADD CONSTRAINT emailid_unique UNIQUE (emailid);


ALTER TABLE ONLY public.experiment
    ADD CONSTRAINT experiment_pkey PRIMARY KEY (experimentid);



ALTER TABLE ONLY public.users
    ADD CONSTRAINT profileid_unique UNIQUE (profileid);


ALTER TABLE ONLY public.profileuser
    ADD CONSTRAINT profileuser_pkey PRIMARY KEY (profileno);



ALTER TABLE ONLY public.simapp
    ADD CONSTRAINT simapp_pkey PRIMARY KEY (said);



ALTER TABLE ONLY public.usersession
    ADD CONSTRAINT simappid_unique UNIQUE (simappid);


ALTER TABLE ONLY public.userexperimentinvite
    ADD CONSTRAINT userexperimentinvite_pkey PRIMARY KEY (inviteid);



ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (userid);


ALTER TABLE ONLY public.usersession
    ADD CONSTRAINT usersession_pkey PRIMARY KEY (sessionid);


ALTER TABLE ONLY public.versions
    ADD CONSTRAINT versions_pkey PRIMARY KEY (versionid);


ALTER TABLE ONLY public.webgazer
    ADD CONSTRAINT webgazer_pkey PRIMARY KEY (wgno);


ALTER TABLE ONLY public.usersession
    ADD CONSTRAINT wgid_unique UNIQUE (wgid);


ALTER TABLE ONLY public.experiment
    ADD CONSTRAINT experiment_exptadminid_fkey FOREIGN KEY (exptadminid) REFERENCES public.users(userid);


ALTER TABLE ONLY public.userexperimentinvite
    ADD CONSTRAINT experimentid_experiment_fk FOREIGN KEY (experimentid) REFERENCES public.experiment(experimentid);


ALTER TABLE ONLY public.profileuser
    ADD CONSTRAINT profileuser_users_fk FOREIGN KEY (profileid) REFERENCES public.users(profileid);


ALTER TABLE ONLY public.simapp
    ADD CONSTRAINT simapp_simappid_fk FOREIGN KEY (simappid) REFERENCES public.usersession(simappid);



ALTER TABLE ONLY public.userexperimentinvite
    ADD CONSTRAINT userexperimentinvite_exptadminid_fkey FOREIGN KEY (exptadminid) REFERENCES public.users(userid);


ALTER TABLE ONLY public.userexperimentinvite
    ADD CONSTRAINT userexperimentinvite_userid_fkey FOREIGN KEY (userid) REFERENCES public.users(userid);


ALTER TABLE ONLY public.usersession
    ADD CONSTRAINT usersession_experimentid_fk FOREIGN KEY (experimentid) REFERENCES public.experiment(experimentid);


ALTER TABLE ONLY public.usersession
    ADD CONSTRAINT usersession_userid_fk FOREIGN KEY (userid) REFERENCES public.users(userid);



ALTER TABLE ONLY public.experiment
    ADD CONSTRAINT versionid_versions_fk FOREIGN KEY (versionid) REFERENCES public.versions(versionid);


ALTER TABLE ONLY public.userexperimentinvite
    ADD CONSTRAINT versionid_versions_fk FOREIGN KEY (versionid) REFERENCES public.versions(versionid);


ALTER TABLE ONLY public.webgazer
    ADD CONSTRAINT webgazer_wgid_fk FOREIGN KEY (wgid) REFERENCES public.usersession(wgid);


CREATE PROCEDURE public.addexpmnt(expname character varying, expadminid integer, expdt date, expjs text[], vno integer, nparams integer, dparams text[], iparams text[], lparams text[])
    LANGUAGE plpgsql
    AS $$
Declare 
	    vid integer;
       --start transaction
       Begin
	    
             -- create version of experiment
               insert into versions(versionno,noofparams,doubleparams,intparams,links) 
               values (vno, nparams,dparams, iparams,lparams );
             
             --get the versionid value    
                 select max(versionid) into vid from versions;
	raise notice 'The max is %',vid;
					   
            -- create experiment 
               insert into experiment(experimentname, ExptadminId,  exptexpirydt, exptjslink ,versionid) 
               values ( expname,expadminid,expdt,expjs,vid);
              
               commit;
   --end transaction
       end;
$$;


ALTER PROCEDURE public.addexpmnt(expname character varying, expadminid integer, expdt date, expjs text[], vno integer, nparams integer, dparams text[], iparams text[], lparams text[]) OWNER TO postgres;


CREATE PROCEDURE public.addprofile(uid integer, gen character varying, agegrp integer, ind character varying, hidegree integer, ptyp character varying)
    LANGUAGE plpgsql
    AS $$
 Declare 
	   n integer;
                   
 --start transaction
begin
   select  profileid  into n from users where userid = uid;
   raise notice 'Count: %', n;
 
  -- check if such email registered 
  if ( n <> 0) then   
  -- add profile to that profileid

    insert into profileuser(profileid ,gender, agegroup,industry,highestdegree, profile_type )
    values (n,gen, agegrp, ind, hidegree , ptyp);
   
  commit;
  end if;
         
end;$$;


ALTER PROCEDURE public.addprofile(uid integer, gen character varying, agegrp integer, ind character varying, hidegree integer, ptyp character varying) OWNER TO postgres;


CREATE FUNCTION public.adusrtab(expid integer) RETURNS TABLE(userid bigint, profile_type character varying, isusernew boolean, industry character varying, sessionid bigint, sdate date)
    LANGUAGE sql
    AS $$	
       --start transaction
	    SELECT * FROM getallprtcpt(expid) ORDER BY getallprtcpt.userid ASC;
$$;


ALTER FUNCTION public.adusrtab(expid integer) OWNER TO postgres;


CREATE FUNCTION public.getallprtcpt(expid integer) RETURNS TABLE(userid bigint, profile_type character varying, isusernew boolean, industry character varying, sessionid bigint, sdate date)
    LANGUAGE sql
    AS $$
    --start trasaction

      		 
	   SELECT u.userid, p.profile_type, u.isusernew, p.industry , s.sessionid, s.sdate 
	    FROM profileuser p  JOIN users u ON p.profileid  = u.profileid  
		JOIN usersession s   ON s.userid = u.userid  
		JOIN experiment e  ON s.experimentid = e.experimentid               
        WHERE  s.experimentid = expid
        UNION
       (SELECT u.userid, p.profile_type, u.isusernew, p.industry , NULL as sessionid, NULL as sdate   
        FROM profileuser p  JOIN users u ON p.profileid  = u.profileid  
		JOIN usersession s   ON s.userid = u.userid 
		WHERE u.userid not in (SELECT s.userid  FROM  usersession s where  s.experimentid = expid))
        UNION
       (SELECT u.userid, p.profile_type, u.isusernew, p.industry , NULL as sessionid, NULL as sdate   
        FROM profileuser p  JOIN users u ON p.profileid  = u.profileid  
        WHERE u.isusernew = false AND u.userid NOT IN (select userid from usersession))
        UNION
       (SELECT userid, NULL as profile_type, isusernew, NULL as industry , NULL as sessionid, NULL as sdate  
        FROM  users WHERE isusernew = true AND account_typeid = 3 AND account_status = true AND userid NOT IN (select userid from usersession) );
	
	 --- end of transaction
   $$;


ALTER FUNCTION public.getallprtcpt(expid integer) OWNER TO postgres;


CREATE PROCEDURE public.inviteusertoexpt(uid integer, eid integer, vid integer, eadminid integer)
    LANGUAGE plpgsql
    AS $$
       Declare 
	    ct integer;
       --start transaction
       Begin

             -- check if that user was invited to expt by that eadmin already
             select count(*) into ct from userexperimentinvite
             where userid =  uid and experimentid = eid 
             and versionid =vid and exptadminid =eadminid;
             raise notice 'The count is %',ct;	    
           
              
             if(ct = 0) then
             -- add as new user invite 
              insert into userexperimentinvite (userid, experimentid, versionid, exptadminid)
              VALUES(uid, eid, vid, eadminid); 
              
             else
              -- user has been invited to expt already by eadminid so upt status of allowed times
              update  userexperimentinvite
              set allowedtimes = allowedtimes+1
              where userid =  uid and experimentid = eid 
              and versionid =vid and exptadminid =eadminid;
             
            --end transaction
               commit;
             end if;

  end;$$;


ALTER PROCEDURE public.inviteusertoexpt(uid integer, eid integer, vid integer, eadminid integer) OWNER TO postgres;

CREATE PROCEDURE public.updtuserssn(uid integer, eid integer, comple boolean, INOUT ssnid integer DEFAULT 0)
    LANGUAGE plpgsql
    AS $$
Declare 
	   
		srvdt date;
		srvtm time;
		
       --start transaction
   Begin
                 raise notice 'The value of uid and eid is % and %', uid, eid;
              		  
             -- add participant and session 
                 insert into usersession(userid, experimentid,sdate, stime, complete)
	             values (uid,eid,CURRENT_DATE, CURRENT_TIME,comple);
            
                 select max(sessionid) into ssnid from   UserSession 
                 where   userid = uid and  experimentid = eid;    

            -- experiment completed and update completedtimes				 
	 UPDATE UserExperimentInvite 
	 SET CompletedTimes = CompletedTimes + 1 
	 WHERE  userid = uid and  experimentid = eid;
            
           -- end transaction
              commit;
             	  
         end;$$;


ALTER PROCEDURE public.updtuserssn(uid integer, eid integer, comple boolean, INOUT ssnid integer) OWNER TO postgres;

SET default_tablespace = '';

SET default_table_access_method = heap;


