  $(document).ready(function(){   
    jQuery.fn.expt_simulate=function(obj)
    {
      var script = document.createElement('script');
       script.type = 'text/javascript';
       script.src = 'https://cdn.plot.ly/plotly-2.3.1.min.js'; 
       document.head.appendChild(script); 
var t0 = 0.0;
var dt= 0.01;
var K_CL = 1, tau_CL = 10;
var y0 = [0.0,(K_CL/tau_CL)];
var yn = getData(y0,t0,dt);
var K = [K_CL];
var cnt = 0;
var size = 1000;

//assume what values in link columns (any number of columns) which will be same in all rows of data
var link1 ="simulation website1";
var link2 ="flight simulator.com";
 Plotly.newPlot('chart',[{y:[yn[0]],type:'line'}]);
 alert("Intialization done..");
  y0 = [0.0,(K_CL/tau_CL)];
  yn = getData(y0,t0,dt);
  interval = setInterval(function(){
    yn = getData(yn,t0+cnt*dt,dt);
    var d = new Date();
    rtimer = d.getTime();
    obj.dblvalue=[yn[0], (yn[0]+0.2),(yn[0]+0.1)]; /**double value**/
    obj.intvalue=[cnt, (cnt +4), (cnt -4)];      /**Integer value */
    obj.linkvalue=[link1,link2];           /** link array value* */
    obj.rtime = [rtimer];              /**time value**/
    //store data
    $(this).storedata();
    Plotly.extendTraces('chart',{y:[[yn[0]]]},[0]);
    cnt++;

    if(cnt > size){
        Plotly.relayout('chart',{
            xaxis: {
                range: [(cnt-size),cnt]
            }
          
        });
    }

},0);

localStorage.setItem('ivals',interval);  

function getData(y1,t1,dt) {
// Integrate up to tmax:
var y = []
//integrator.step()

y[0] = y1[0]+0.05;
y[1] = y1[1]+0.05;
return y;  
}
}
});