$(document).ready(function(){ 
  var data = {};
  var cnt = 0;
  var woff, won, delayed,et;
  var row = 0;
  var rtimer;
  var formData;
  var uids, eids,cids;
  cids = 0;
  data.table2 = []; /**double  precision parameter values array**/
  data.table3 = []; /**Integer precision parameter values array**/ 
  data.table4 = []; /**link  parameter values array**/
  data.tablertime = []; /** real time */
  var obj = {
      id: cnt,
      dblvalue:[], /**double value */
      intvalue:[],  /**int value */
      linkvalue:[],   /**link value */
      rtime :[] /**time value */
    };
  
   //client experiment global vars 
  //   Read from globalvars file.
    var yn = 0, t0, dt;
    var t0 = 0.0;
    var dt= 0.01;
    var K_CL = 1, tau_CL = 10;
    var y0 ;
    var yn ;
    var K = [K_CL];
    var cnt = 0;
    var size = 1000;
    var interval;
   
  
    
     
    /** start function  **/
    $( "#start" ).click(function(){
        $(this).expt_simulate(obj);
     });
  
    /** stop function **/
    $( "#stop" ).click(function() {
        interval = localStorage.getItem('ivals');
        clearInterval(interval);
        $(this).expt_terminate(); 
        cids=1;
        $(this).writetodb();
      }); /** end of stop**/
  
 
  
  
  
  
  //fixed
  jQuery.fn.writetodb =  function() {
      //data object pushed into formdata which passed to api to write to db
        formData = new FormData();  
        uids= localStorage.getItem('uid');
        eids= localStorage.getItem('eid');   
        apiurls = localStorage.getItem('apiurl');
        clienturl = localStorage.getItem('clienturl');
        localStorage.clear();
        formData.append("dblvalue",JSON.stringify(data.table2));
        formData.append("intvalue",JSON.stringify(data.table3));
        formData.append("linkvalue",JSON.stringify(data.table4));
        formData.append("rtime",JSON.stringify(data.tablertime));
        $(this).checknetstat();
  } 
  
  //fixed
  jQuery.fn.storedata = function()
  {  //data stored from obj to data object.
        data.table2.push(obj['dblvalue']); /**double parameters value array**/
        data.table3.push(obj['intvalue']); /**Integer parameters value array */
        data.table4.push(obj['linkvalue']); /**Integer parameters value array */
        data.tablertime.push(obj['rtime']); /** real time**/
        row =row+1;
  }
  
  //fixed
  jQuery.fn.abort =  function() {
    cids=0;
    $(this).writetodb();
  }
  
  //fixed  
  jQuery.fn.checknetstat =  function() { 
    var tdelay, won = 0, wof = 0;
    netconn();
    function netconn() {
      if(navigator.onLine ){
          var d1 = new Date();
          won = d1.getMinutes();
          tdelay = won -wof;
          if (tdelay<5000) {
            alert("Experiment data is being stored..");
              $.ajax({
                  url : apiurls +"/Senddata/"+uids +"/"+eids+"/"+cids,
                  type: "POST",
                  data : formData,
                  dataType    : 'json',
                  processData : false,
                  contentType: false,
                  success: function(result)
                  {
                    //console.log("success");    
                   },
                  completed: function(result)
                  {
                    //console.log("completed");    
                   }, 
                   error: function (jqXHR, textStatus, errorThrown)
                  {
                  }
  
                });
             } 
             alert("Experiment completed successfully.");
             localStorage.setItem('cflag',true);
             alert("Login to run the next Experiment");
             window.close();
           }// if online
          else {
              var d = new Date();
              wof = d.getTime();
               }
          }  
          window.addEventListener("offline",netconn);
          window.addEventListener("online",netconn);
     }
    }
 );