import React, { Component } from 'react';
import axios from 'axios';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
export default class Forgotpwd extends Component {
   constructor() {
      super();
      this.state = {
        email: '' 
      
      };
    }
    validateemail(event){
        var em=event.target.value;
       const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
       const isOk = re.test(em);
       if (isOk) {
         document.getElementById("email").innerHTML="Valid";
         document.getElementById("email").style.color = "green"
       } else{
         document.getElementById("email").innerHTML="Invalid"  
         document.getElementById("email").style.color = "red"
       }
      }
   
    onSubmit = (e) => {
      e.preventDefault();
      const { email } = this.state;
      axios.post(process.env.REACT_APP_API_URL+'/sendmail',{ email }).then((res) => 
      {
        if (res.status==500)
        {
        toast.error('Error')
        }
        else if(res.status==203)
        {
          toast.error(res.data)
          this.props.history.push({pathname:'/Home'});
        }
        else 
        {
          toast.info('Password link sent to your Email');
          this.props.history.push({pathname:'/Home'});
        }
      })
    }
  
render() {
    return (
      <div class="card login">
      <div class="card-body login-card-body">
      <div>
         <form onSubmit={this.onSubmit}>
              <div class="form-group">
                <label for="isbn">Email Id:</label>
                <input type="text" class="form-control" name="email"  onInput={this.validateemail} onChange={(e) => {this.setState({email : e.target.value})}} placeholder="email" />
              </div>
              <p>
                 <div id="email"></div>  
              </p>
              <button type="submit" class="btn btn-primary" disabled={!this.state.email}>Submit</button>
         </form>
       </div>
       </div>
       </div>
    );
 }
}
