import React,{Component} from 'react'
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import CreatExpt from '../Eadmin/createexperiment';
import ChangeExpt from '../Eadmin/ChangeExptstatus';
import ManageExpt from '../Eadmin/ManageExpt';
import AddUser from '../Eadmin/AddUser';
import Reset from '../Shared/Reset'
import Logout from '../Shared/Logout';
import View from '../Eadmin/ViewExpData'

import { toast } from 'react-toastify';
import Footer from './Footer';
export default class Eadmin extends Component{
    componentDidMount() 
    {
        if(((sessionStorage.getItem('userid'))==null)||((sessionStorage.getItem('acctype'))!=2))
               {
                    toast.warning('Unauthorized');
                    sessionStorage.clear();
                    this.props.history.push({pathname:'/Home'});
               }
    }
    
    render(){
        return(  
            <div>
            <Router>
                   <div>
                   <div>
               <nav class="main-header navbar navbar-expand navbar-white navbar-light">
                    <ul class="navbar-nav">
                        <li class="nav-item d-none d-sm-inline-block">
                        <Link className="nav-link" to={"/CreatExpt"}>Create Experiment</Link>
                        </li>
                        <li class="nav-item d-none d-sm-inline-block">
                        <Link className="nav-link" to={"/ManageExpt"}>Manage Expt</Link>  
                        </li> 
                        <li class="nav-item d-none d-sm-inline-block">
                        <Link className="nav-link" to={"/Reset"}>Reset Password</Link>  
                        </li>
                        <li className="l">
                        <Link className="nav-link" to={"/Logout"}><strong> Welcome {sessionStorage.getItem('username')} !</strong> LOG OUT</Link>
                        </li> 
                        
                    </ul>
                    
              </nav>
            </div>
             
    <Switch>
    
    <Route path="/CreatExpt"component={CreatExpt}/> 
    <Route path="/ChangeExpt"component={ChangeExpt}/> 
    <Route path="/ManageExpt"component={ManageExpt}/> 
    <Route path="/ViewData"component={View}/> 
    <Route path="/Reset" component={Reset} />
    <Route path="/AddUser"component={AddUser}/> 
    <Route path="/Logout"component={Logout}/> 
    </Switch>
    <Footer></Footer>
                   </div>
                  
            </Router>
            </div>
        )
    }

}