import React,{Component} from 'react'
import './login.css'
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';


export default class Header extends Component{
  constructor(props) {
    super(props);
    this.state = {
      uname :'',
      unam: '',
      uemail:'',
      pswd: '',
      ermesg :'',
      id:''
     }
   
}

resetlogin(e)
 {
    this.props.history.push('/Login');
 }

LoginAuth(r) {
    r.preventDefault();
    fetch(process.env.REACT_APP_API_URL+'/UserAuth0', {
      method: 'POST',
        headers: {
            "Accept": "application/json",
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
           uemail : this.state.uemail,
           passwd : this.state.pswd 
        })
    }).then ((resp) => {
      //
           if (resp.status === 200) {
                resp.json().then((result) => {
                //check if he is active account and not deleted user
                if (result[0]["account_status"]===true) {
                this.setState({unam:result[0]["uname"] }); 
                sessionStorage.setItem('username',result[0]["uname"]);
                sessionStorage.setItem('userid',(result[0]["userid"]));
                sessionStorage.setItem('uemail',(result[0]["emailid"]));
                sessionStorage.setItem('acctype',(result[0]["account_typeid"]));
                this.setState({ermesg :''});
                if (result[0]["account_typeid"] === 1) {
                  this.props.history.push({pathname:'/Sadmin'});
                }
                else if(result[0]["account_typeid"] === 2) {
                  this.props.history.push({pathname:'/Eadmin'});
                }
                else if((result[0]["account_typeid"] === 3)&&(result[0]["isusernew"]===false)) {
                  this.props.history.push({pathname:'/MyExper'});
                }
                else
                {
                  if((result[0]["account_typeid"] === 3)&&(result[0]["isusernew"]===true))
                  this.props.history.push({pathname:'/Terms'});
                }
              } //if of active acc
              else {
                  toast.error('Invalid user or password');
                   }
            })
          }
          else {
            toast.error('Invalid user or password');
            } 
       }) ; 
}

 render(){
        return(
      <div class="card login">
      <div class="card-body login-card-body">
      <p class="login-box-msg">Sign in to start your session</p>
      <form >
      <div class="input-group mb-3">
      <input type="text" className="form-control"  placeholder="Enter Emailid"  onChange={(e) => {this.setState({uemail : e.target.value}) }}  />
        <div class="input-group-append">
          <div class="input-group-text">
            <span class="fas fa-envelope"></span>
          </div>
        </div>
      </div>
    <div class="input-group mb-3">
    <input type="password" className="form-control" placeholder="Enter password" onChange={(e) => {this.setState({pswd : e.target.value}) }} />
      <div class="input-group-append">
        <div class="input-group-text">
          <span class="fas fa-lock"></span>
        </div>
      </div>
</div>
  <div class="row">
    <div class="col-8">
      
      <p class="mb-1">
        <a href="/Forgotpwd">I forgot my password</a>
      </p>
  </div>
  <span className="loginstat">{this.state.ermesg}</span>
<div class="col-10">

<button type="submit" className="btn btn-primary mr-1" onClick={(e) => this.LoginAuth(e)} >Sign in</button>  
<button type="submit" className="btn  btn-primary " onClick={(e) => this.resetlogin(e) } >Reset</button>
</div>
         </div>
            </form>
            </div>
            </div>
                )
    }
}