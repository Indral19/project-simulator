import React,{Component} from 'react'

 import AdduserfromCSV from '../Sadmin/AdduserfromCSV'
 import Deleteuser from '../Sadmin/Deleteuser'
 import Reset from '../Shared/Reset'
 import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
 import { toast } from 'react-toastify';
 import Logout from '../Shared/Logout';
export default class Sadmin extends Component{

    componentDidMount() 
    {
            if(((sessionStorage.getItem('userid'))==null)||((sessionStorage.getItem('acctype'))!=1))
               {
                  toast.warning('Unauthorized');
                  sessionStorage.clear();
                  this.props.history.push({pathname:'/Home'});
               }
        }
    
    render(){
        return(
         <Router>
           <div>
               <div>
               <nav class="main-header navbar navbar-expand navbar-white navbar-light">
                    <ul class="navbar-nav">
                        <li class="nav-item">
                        {/* <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>  */}
                        </li>
                        <li class="nav-item d-none d-sm-inline-block">
                        <Link className="nav-link" to={"/AddUsers"}>Add Users</Link>
                        </li>
                        <li class="nav-item d-none d-sm-inline-block">
                        <Link className="nav-link" to={"/ManageUsers"}>Manage Users</Link>
                        </li>
                        <li class="nav-item d-none d-sm-inline-block">
                        <Link className="nav-link" to={"/Reset"}>Reset Password</Link>  
                        </li>
                        <li class="l">
                        <Link className="nav-link" to={"/Logout"}><strong> Welcome {sessionStorage.getItem('username')} !</strong> LOG OUT</Link>
                        </li>
                    </ul>
              </nav>
            </div>
    
    <Switch>
    <Route path="/AddUsers" component={AdduserfromCSV} />
    <Route path="/ManageUsers"component={Deleteuser}/> 
    <Route path="/Reset" component={Reset} />
   
    <Route path="/Logout"component={Logout}/> 
    </Switch>
   
    </div>
    </Router>
                
        )
    }
} 
        