import React, { Component } from "react";

import http from "../http-common";
import { toast } from "react-toastify";

export default class Resetpswd extends Component 
{
   constructor(props)
    {
      super(props);
      this.state = 
      {
        emailid :'',
        conwd:'',
        paswd:'',
        profile:Boolean 
      }
     
    }
  componentDidMount() 
    {
        if((sessionStorage.getItem('userid'))==null)
        {   
            toast.warning('Unauthorized')
            this.props.history.push({pathname:'/Home'});
        }
        else
        {
        this.setState({emailid:sessionStorage.getItem('uemail')})
        }
      
    }
   
   
   validate(event) 
{ 
    var pass = event.target.value;
    const re = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})");
    const isOk = re.test(pass);
    if (isOk) 
    {
      document.getElementById("pwd").innerHTML="strong";
      document.getElementById("pwd").style.color = "green"
    } 
    else
    {
      document.getElementById("pwd").innerHTML="Weak"  
      document.getElementById("pwd").style.color = "red"
    }
}

validate1(event) 
{
    var pass = event.target.value;
    const re = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})");
    const isOk = re.test(pass);
    if (isOk) 
    {
      document.getElementById("pwd1").innerHTML="strong";
      document.getElementById("pwd1").style.color = "green"
    } 
    else
    {
      document.getElementById("pwd1").innerHTML="Weak"  
      document.getElementById("pwd1").style.color = "red"
    }
}
changepwd(e)
{
  e.preventDefault();
  const { emailid,paswd,conwd } = this.state;
  if(this.state.paswd != this.state.conwd)  
    {   
         toast.error("Passwords did not match");  
    } 
  else
    {  
        http.post('/resetpwd',{ emailid,paswd }) .then
        (res =>
        {
            if(res.status===500)
            {
              toast.error('Error')
            }
            else 
            {
              toast.success('password changed');
              this.props.history.push({pathname:'/Logout'}); 
            }
        }   
        )
    }
}

render() 
{
  return(
    <div>
      <div class="card login">
      <div class="card-body login-card-body">
                  <form >
                        <div>
                             <h4 class="text-center">Reset Password</h4>
                            
                                      <div>
                                              <label class="label">Email</label>
                                              <input type="text" className="form-control"  placeholder="Enter Email" value={this.state.emailid}   />
                                              <p>
                                              <div id="email"></div>  
                                              </p>
                                      </div>
                                      <div className="form-group">
                                            <label class="label">Password</label>
                                            <input type="password" className="form-control" placeholder="Enter password" onInput={this.validate  } onChange={(e) => {this.setState({paswd : e.target.value})}} />
                                            <p>
                                            <div id="pwd"></div>  
                                            </p>
                                      </div>
                                     <div>
                                            <label class="label">Confirm Password</label>
                                                                <input type="password" className="form-control" placeholder="Enter password" onInput={this.validate1  } onChange={(e) => {this.setState({conwd : e.target.value})}} />
                                                                <p>
                                                                <div id="pwd1"></div>  
                                                                </p>
                                      </div>  
                                      <div >
                                            <button type="submit" className="btn btn-primary"  disabled={!this.state.emailid || !this.state.paswd||!this.state.conwd} onClick={(e) => this.changepwd(e)} >submit</button> 
                                      </div>
                            </div> 
                   </form>
  </div>
  </div>
  </div>
  )
}
}
