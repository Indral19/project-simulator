import React,{Component} from 'react';


import {  Link } from "react-router-dom";

export default class PHeader extends Component{
    render(){
        return(
          
            <div>
                <div>
                    <nav class="main-header navbar navbar-expand navbar-white navbar-light">
                        <ul class="navbar-nav">
                        {/* <li class="nav-item">
                            <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a> 
                        </li> */}
                        <li class="nav-item d-none d-sm-inline-block">
                        <Link className="nav-link" to={"/About"}>About</Link>
                        </li> 
                           <li class="nav-item d-none d-sm-inline-block">
                        <Link className="nav-link" to={"/Myprofile"}>Myprofile</Link>
                        </li>   
                        <li class="nav-item d-none d-sm-inline-block">
                        <Link className="nav-link" to={"/Reset"}>Password</Link>  
                        </li>
                        {/* <li class="nav-item d-none d-sm-inline-block">
                        <Link className="nav-link" to={"/Mytask"}>MyTask</Link>  
                        </li> */}
                         <li className="l">
                        <Link className="nav-link" to={"/Logout"}> <strong>Welcome {sessionStorage.getItem('username')}</strong> ! LOG OUT</Link>
                        </li> 
                        </ul>
                        
                    </nav>
                    
                </div>
                
            </div>
            
            
        )
        }
}