// import logo from './logo.svg';
import './App.css';

import { BrowserRouter as Router,  Route ,Redirect} from "react-router-dom";
import './Shared/login.css';
import Resetpswd from './Sadmin/Resetpswd'
import Login from './Shared/login'
import Forgot from './Shared/Forgotpwd'
import Reset from './Shared/Reset'
import Sadmin from './Shared/Sadmin'
import Eadmin from "./Shared/Eadmin";
import Home from "./Shared/Home";

import Logout from "./Shared/Logout";

import Deleteuser from "./Sadmin/Deleteuser";
import src from "./Eadmin/src";
import Terms from "./Participants/Terms";
import Mytask from "./Participants/Mytask";
import Myprofile from "./Participants/Myprofile";
import MyExper from "./Participants/MyExperiment";
import About from "./Participants/About";

function App() {
  return (
     <Router >
    <div className="wrapper">
   
      <switch>
    <Route path="/Sadmin" component={Sadmin} />
    <Route path= "/Home" component={Home} />
    
    <Route path="/Logout" component={Logout} />
    <Route path="/ResetPassword" component={Resetpswd} />
    <Route path="/Reset" component={Reset} />
  
    <Route path="/Eadmin" component={Eadmin}/>
     <Route path="/Login" component={Login}/> 
    <Route path="/ManageUsers" component={Deleteuser}/>
    <Route path="/Terms" component={Terms}/>
    <Route path="/Myprofile" component={Myprofile}/>
    <Route path="/src/:id" component={src}/>  
    <Route path="/Mytask/:id" component={Mytask}/>
    <Route path="/About" component={About}/>
    <Route path="/MyExper" component={MyExper}/>
    <Route path="/Forgotpwd" component={Forgot}/>
    <Route exact path="/"
    render={()=>
      {
        return  (
          <Redirect to="/Home" />
        )
      }
    } 
    /> 
   </switch> 
   
    </div>
     </Router>  
  );
}

export default App;