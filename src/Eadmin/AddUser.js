import React, { Component } from "react";
import { toast } from 'react-toastify';
export default class AddUser extends Component {
   constructor() {
      super();
      this.state = {
         expnam :'',
         emailid:'',
         expid :0,
         adminid :0,
         verid : 0,
         uid :0,
         exptmnt :[], 
         expt1:[],
         temp:'',
         resultnull : false
       }
   }

   formatYesNo(value){
      return value===0 ? 'No' : 'Yes';
    
    }

    
    dateprinter=(date)=>{
      if (date === null || date === undefined) return null
      else return date.slice(0,10);
    }  

 sendemail() {
    //send email to user.
      fetch( process.env.REACT_APP_API_URL+'/usernamebyid', {
                method: 'POST',
                headers: {
                 "Accept": "application/json",
                 'Content-Type': 'application/json'
                 },
                body: JSON.stringify ({
                   uid: this.state.uid
                })
             }).then ((resp) => {
                  if (resp.status === 200) {
                        resp.json().then((result) => {
                              this.setState({uname:result[0].uname})
                              this.setState({emailid:result[0].emailid})
                               fetch(process.env.REACT_APP_API_URL+'/smailadusr', {
                                        method: 'POST',
                                        headers: {
                                          "Accept": "application/json",
                                          'Content-Type': 'application/json'
                                        },
                                        body: JSON.stringify ({
                                        emailid: this.state.emailid,
                                        ename: this.state.expnam,
                                        uname: this.state.uname
                                        })
                                  }).then ((resp) => {
                                                 if (resp.status === 200) {
                                                 }
                                                 else{
                                                  // toast.error("Email is not sent");
                                                 }
                                               });
                             });
                   }
                   else {
                        }                 
                 });
          }

   componentDidMount() {
      this.setState({adminid: sessionStorage.getItem('userid')});
      this.state.expid = sessionStorage.getItem('expid');
      this.setState({expnam: sessionStorage.getItem('expnam')});
      // call the api to get table details..
      if(((sessionStorage.getItem('userid'))==null)||((sessionStorage.getItem('acctype'))!=2))
               {
                  toast.warning('Unauthorized');
                  sessionStorage.clear();
                  this.props.history.push({pathname:'/Home'});
               }
      else {         
      fetch(process.env.REACT_APP_API_URL+'/getusrfnf1', {
          method: 'POST',
          headers: {
              "Accept": "application/json",
              'Content-Type': 'application/json'
          },
          body: JSON.stringify ({
              expid : this.state.expid
           })
      }).then ((resp) => {
          if (resp.status === 200) {
                  resp.json().then((result) => {
                  this.setState({exptmnt:result});
                  this.setState({resultnull : false})
               })
            }
          else {
            // no users 
               }
        });
      }
   }
   

   Invite(e,f) {
    e.preventDefault();
    this.setState({uid: f});
    // call api to get versionid of experiment 
    fetch(process.env.REACT_APP_API_URL+'/getversionid', {
                     method: 'POST',
                     headers: {
                           "Accept": "application/json",
                           'Content-Type': 'application/json'
                       },
                     body: JSON.stringify ({
                      eid: this.state.adminid,
                      expid : this.state.expid,
                    })
                  }).then ((resp) => {
                    if (resp.status === 200) {
                      resp.json().then((result) => {
                         this.setState({verid : result[0]["versionid"]});
                         // call api make entry in userexptinvite table and send invite to their emailid 
                         fetch(process.env.REACT_APP_API_URL+'/InviteuserExpt', {
                            method: 'POST',
                            headers: {
                                  "Accept": "application/json",
                                  'Content-Type': 'application/json'
                              }, 
                            body: JSON.stringify ({
                               eid: this.state.adminid,
                               expid : this.state.expid,
                               verid: this.state.verid,
                               uid: this.state.uid
                            })
                          }).then ((resp) => {
                            if (resp.status === 200) {
                                toast.success("User Invited and mail sent");
                                //send email to user.
                                this.sendemail();
                            }
                            else {
                              // toast.error('Invitation Failed');
                              }
                          });
                        });
                    }
                    else {
                      //Err No version id
                    }
                  });
           }


      render() {
        if(!this.state.resultnull) {
        return (
           <div>
           <div class="row">
          <div class="col-10">
           <div class="card">
             <div class="card-header">
               <h3 class="text-center"><p className ="expstat">Experiment name : {this.state.expnam}   </p></h3>
     
               <div class="card-tools">
                 <div class="input-group input-group-sm" >
                   <input type="text" name="table_search" class="form-control float-right" placeholder="Search"/>
     
                   <div class="input-group-append">
                     <button type="submit" class="btn btn-default">
                       <i class="fas fa-search"></i>
                     </button>
                   </div>
                 </div>
               </div>
             </div>
              <div class="card-body table-responsive p-0">
               <table class="table table-hover text-nowrap">
                 <thead class="header">
                   <tr>
                     <th>UserID</th>  
                     <th>Profession</th>
                     <th>IsUsernew</th>
                     <th>Industry</th>
                     <th>SessionID</th>
                     <th>Date</th>
                     <th>Invite</th>
                   </tr>
                 </thead>
                 <tbody>
                 {this.state.exptmnt.map(exptmnt =>
                   <tr>
                     {exptmnt.isusernew ? <td className="loginstat">{exptmnt.userid}</td> : <td>{exptmnt.userid}</td>}
                     <td>{exptmnt.profile_type}</td>
                     {exptmnt.isusernew ? <td className="loginstat">{this.formatYesNo(exptmnt.isusernew)}</td> : <td>{this.formatYesNo(exptmnt.isusernew)}</td>}
                     <td>{exptmnt.industry}</td>  
                     <td>{exptmnt.sessionid}</td>
                     <td>{this.dateprinter(exptmnt.sdate)}</td>
                     <td><button type="submit" className="btn  btn-primary " onClick={(e) => this.Invite(e,exptmnt.userid) } >Invite</button></td>
                   </tr> 
                 )}
                 </tbody>
                 </table>
                 </div>
                 </div>
                 </div>
                 </div>
                 </div>
             );
           } 
           else {
              return (
               <div class="row">
               <div class="col-10">
                 <div class="card">
                   <div class="card-header">
                     <h3 class="text-center"><p className ="expstat">Experiment name : {this.state.expnam}   </p></h3>
           
                     <div class="card-tools">
                       <div class="input-group input-group-sm" >
                         <input type="text" name="table_search" class="form-control float-right" placeholder="Search"/>
           
                         <div class="input-group-append">
                           <button type="submit" class="btn btn-default">
                             <i class="fas fa-search"></i>
                           </button>
                         </div>
                       </div>
                     </div>
                   </div>
                 
                 
                    <div class="card-body table-responsive p-0">
                     <table class="table table-hover text-nowrap">
                       <thead class="header">
                         <tr>
                           <th>UserID</th>
                           <th>Profession</th>
                           <th>IsUsernew</th>
                           <th>Industry</th>
                           <th>SessionID</th>
                           <th>Date</th>
                           <th>Invite</th>
                         </tr>
                       </thead>
                       <tbody>
                       </tbody>
                       </table>
                       <div className="form-group">
                         <h6><p className="expstat1">--------------  N o   R e c o r d s -------------- </p></h6>
                        </div>    
                       </div>
                       </div>
                    </div>
                    </div>
                 );
            }
        
           }

}   