import React, { Component } from 'react';
import { toast } from 'react-toastify';
//  import './index.css';

export default class ChangeExptstatus extends Component {
   constructor() {
      super();
      this.state = {
         expname :'',
         expid :0,
         adminid :0,
         estatus :'',
         ermesg:'',
         smesg :''
        }
   }

   componentDidMount() {
      this.state.adminid= sessionStorage.getItem('userid');
      this.state.expid=sessionStorage.getItem('expid');
      this.state.expname = sessionStorage.getItem('expnam');
      if(((sessionStorage.getItem('userid'))==null)||((sessionStorage.getItem('acctype'))!=2))
               {
                  toast.warning('Unauthorized');
                  sessionStorage.clear();
                  this.props.history.push({pathname:'/Home'});
               }
      else {         
      fetch(process.env.REACT_APP_API_URL+'/getExpmtstats', {
          method: 'POST',
          headers: {
              "Accept": "application/json",
              'Content-Type': 'application/json'
          },
          body: JSON.stringify ({
              expid :this.state.expid,
           })
      }).then ((resp) => {
          if (resp.status === 200) {
                  resp.json().then((result) => {
                   this.setState({estatus : result[0]["exptactivestatus"]});
                   });
             } 
          else {
              //ERR : experiments active status details not found
               }
      });
     }
    }

    delete(e) {
       e.preventDefault();
       if(!this.state.estatus) {
         this.setState({estatus : false});   
       fetch(process.env.REACT_APP_API_URL+'/changeExptstats', {
          method: 'PUT',
          headers: {
              "Accept": "application/json",
              'Content-Type': 'application/json'
          },
          body: JSON.stringify ({
              expid :this.state.expid,
              value :this.state.estatus
           })
      }).then ((resp) => {
          if (resp.status === 200) {
            toast.success("Experiment Deleted");
            this.props.history.push({pathname:'/Eadmin'}); 
           }
          else {
              toast.error('Experiment NOT Deleted');
              this.setState({estatus : true}); 
              }
      });
    } 
    else { 
      this.setState({estatus : true});
      this.setState({ermesg:'uncheck the box to delete'});
      this.setState({smesg:''});
   }
}

   // will not commit changes to db.
   reset(e) {
      e.preventDefault();
      this.props.history.push({pathname:'/Eadmin'});
   }



   handleInputChange = (e) => {
      this.setState({
         estatus : e.target.checked
       });
   }
 
   

 
render() {
   if(this.state.estatus) {
      return (
      <div>
         <div class="card login">
         <div class="card-body login-card-body">
      <br></br>
        <form >
                  <div className="form-group">
                     <h4 class="text-center"> Manage Experiments </h4>
                     <br></br>
                     <h5> Delete Experiment</h5>
                  </div>   
                  <div className="form-group">
                     <h5>Experiment name : <strong> {this.state.expname}</strong> </h5>
                  </div>    
                  <br></br>
                  <div>
                     <input type="checkbox" id="expstatus" value={this.state.estatus} name="estats" onChange={(e)=> this.handleInputChange(e)} checked/>
                     <span> </span>
                     <label> Experiment Active Status</label>
                  </div>
                  <br></br>
                  <div className="form-group">
                     <label> To Delete Experiment uncheck the box  </label> 
                     <span>   </span> 
                     <label>above and click Delete button. </label>
                  </div>
                  <div>
                     <button type="submit" className="btn btn-dark" onClick={(e) => this.delete(e) } >Delete</button>
                        <span>{"                         "}    </span>
                     <button type="submit" className="btn btn-dark" onClick={(e) => this.reset(e)} >Cancel</button>
                  </div> 
         </form>
      </div>
      </div>
   </div>
      );
   }
   else {
      return (
         <div>
                <div class="card login">
                <div class="card-body login-card-body">
                <br></br>
                     <form >
                     <div className="form-group">
                        <h3> Manage Experiments </h3>
                        <h3> Delete Experiment</h3>
                        <br></br>
                     </div>  
                        <div className="form-group">
                        <h5>Experiment name : {this.state.expname}</h5>
                        </div>    
                        <br></br>
                        <div>
                           <input type="checkbox" id="expstatus" value={this.state.estatus} name="estats" onChange={(e)=> this.handleInputChange(e)} />
                           <span> </span>
                           <label> Experiment Active Status</label>
                        </div>
                        <br></br>
                        <div className="form-group">
                           <label> To Delete Experiment uncheck the box  </label> 
                           <span>   </span> 
                           <label>above and click Delete button. </label>
                        </div>
                        <div>
                           <button type="submit" className="btn btn-dark" onClick={(e) => this.delete(e) } >Delete</button>
                           <button type="submit" className="btn btn-dark" onClick={(e) => this.reset(e)} >Cancel</button>
                        </div>    
                     </form>
                  </div>
                  </div>
       </div>
       );

   }
  }
}