import React, { Component } from 'react';
import http from "../http-common";
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

 export default class src extends Component 
 {
   constructor(props) {
    super(props);
    this.state = {
      experi: {},
      filepath:'',
      no:''
    };
  }
     
componentDidMount() 
{
  const script = document.createElement("script"); 
  script.src=process.env.REACT_APP_PUBLIC_URL+'fixed.js';
  script.onload = () => this.simple();
  document.head.appendChild(script);
          http.get('/JSFile/'+this.props.match.params.id)
                  .then(res => 
                    {
                    this.setState({experi:res.data[0]['exptjslink']});
                    this.setState({no:res.data.length});
                   for(var i of this.state.experi){    
                    const script = document.createElement("script");
                    script.src=process.env.REACT_APP_PUBLIC_URL+i;
                    script.async = true;
                    script.onload = () => this.simple();
                    document.head.appendChild(script);
             }
       },(error) => 
         {
             toast.error('Error');
        }
        );
   } //end of component               
          

  
      simple(){
      }
        

render(){
    return(
        <div>
          <div class="navbar"><span>Real-Time graph</span>
           <div class="wrapper">
           <div id="chart"></div>
            <button type="submit" id="start"  className="btn btn-primary">start</button>
             <button type="submit" id="stop"   className="btn btn-primary">stop</button>
           </div>
           </div>
           </div>
        )
}
   
 }
    
   
   