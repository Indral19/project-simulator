import React, { Component } from 'react';
import Select from 'react-select';
import * as FileSaver from 'file-saver';
import { toast } from 'react-toastify';

export default class ManageExpt extends Component {
  fname1 ="";
  selectd = false; 
  constructor(props) {
    super(props);
      this.state = {
         exptmnt :[{label:'', value:0}],
         adminid : '',
         selectedvalue :{label:'', value:0},
         datas1 :[],
         datas :[],
         exptmnts :[],
         maxsize :0,
         expname :'',
         expid :0
      }
   }

  componentDidMount() {
      this.state.adminid = sessionStorage.getItem('userid');
      sessionStorage.setItem('expnam','');
      if(((sessionStorage.getItem('userid'))==null)||((sessionStorage.getItem('acctype'))!=2))
      {
         toast.warning('Unauthorized');
         sessionStorage.clear();
         this.props.history.push({pathname:'/Home'});
      }
      else {
      fetch(process.env.REACT_APP_API_URL+'/getAllExpmnts', {
          method: 'POST',
          headers: {
              "Accept": "application/json",
              'Content-Type': 'application/json'
          },
          body: JSON.stringify ({
             eid :this.state.adminid
           })

      }).then ((resp) => {
          if (resp.status === 200) {
                  resp.json().then((result) => {
                  this.setState({exptmnt:result});
                  for(var i =0; i<result.length; i++)
                   {
                     this.state.exptmnt[i]={label: result[i]["experimentname"], value : result[i]["experimentid"]}; 
                   }
              } 
          )}
          else {
              //ERR : No experiments created for this experiment admin
              }
      });
     }
    }

  
    handleExptChange = (event) => {
      this.state.selectedvalue ={label: event["label"], value : event["value"]};
      sessionStorage.setItem('expnam',this.state.selectedvalue.label);
      sessionStorage.setItem('expid',this.state.selectedvalue.value);
      this.state.expid=this.state.selectedvalue.value;
      this.selectd = true;
     }
    
    paramdwnld= (e) =>  { 
      e.preventDefault();
      //gets experiment parameters for the selected experiment.
      if (!this.selectd)
      {
         toast.warning('Select experiment');
       }
       else {
      fetch(process.env.REACT_APP_API_URL+'/getallParams', {
        method: 'POST',
        headers: {
            "Accept": "application/json",
            'Content-Type': 'application/json'
        },
        body: JSON.stringify ({
            expid :this.state.expid
         })
      }).then ((resp) => 
     {
        if (resp.status === 200) 
        {       
                resp.json().then((result) => 
                {
                this.state.exptmnts=[];  
                this.setState({datas:result});
                this.fname1= 'PARAMS_'+sessionStorage.getItem('expnam')+'.csv';
                this.setState({maxsize :  Math.max(this.state.datas[0]["doubleparams"].length,this.state.datas[0]["intparams"].length,this.state.datas[0]["links"].length)});
                this.state.exptmnts[0]= "Double "+','+"Integer "+','+"Links";
                for(var i=0; i<this.state.maxsize; i++) {
                  if(this.state.datas[0].doubleparams === null || this.state.datas[0].doubleparams[i]=== undefined || this.state.datas[0].doubleparams.length === 0 || this.state.datas[0].doubleparams[i] === '')  this.state.datas[0].doubleparams[i]="";
                  if(this.state.datas[0].intparams[i]=== undefined || this.state.datas[0].intparams.length === 0 || this.state.datas[0].intparams[i]=== '') this.state.datas[0].intparams[i] ="";
                  if(this.state.datas[0].links[i]=== undefined || this.state.datas[0].links.length === 0 || this.state.datas[0].links[i]=== '') this.state.datas[0].links[i] ="";
                  this.state.exptmnts[i+1]= "\n"+this.state.datas[0].doubleparams[i]+','+this.state.datas[0].intparams[i]+','+ this.state.datas[0].links[i];
                }
                let blob = new Blob([this.state.exptmnts], { type: 'text/csv;charset=utf-8' });
                FileSaver.saveAs(blob, this.fname1);
               })}
                else {
                  //Err: not getting data from server
                }
               }) ;
             }   
          }
          

    //when change expt is clicked.
    handleClick1 = (e) => {
      e.preventDefault();
      if (!this.selectd)
      {
         toast.warning('Select experiment');
       }
       else {
         this.props.history.push({pathname:'/ChangeExpt'});
       }
    };
  
    //Addusers is clicked.  
    handleClick2 = (e) => {
      e.preventDefault();
      if (!this.selectd)
      {
         toast.warning('Select experiment');
       }
       else {
           this.props.history.push({pathname:'/AddUser'});
       }
    };

    //when download expt sim data is clicked
    viewdata = (event) => {
    event.preventDefault();
    if (!this.selectd)
      {
         toast.warning('Select experiment');
       }
       else {
          this.props.history.push({pathname:'/ViewData/'+this.state.expid});
       }
   };
 
render() {
     return (
        <div>
                <div class="card login">
                <div class="card-body login-card-body">
                <br></br>
                        <form>
                              <h4 class="text-center"> Manage Experiments </h4>
                                <br></br>
                                    <div className="form-group">
                                        <label> Current experiment :  </label> <label> <i>{sessionStorage.getItem('expnam')}</i> </label>
                                        <br></br>
                                        <label>Choose an Experiment :   </label>        
                                        <Select options={this.state.exptmnt}  onChange={this.handleExptChange}/>
                                    </div>  
                                    <div className="form-group">
                                    <a href="none"  onClick={this.paramdwnld} >Download parameters as CSV </a>  
                                    </div> 
                                    <div>
                                       <a href="/ChangeExptstatus" onClick={this.handleClick1} >Change Experiment Active status</a>
                                    </div> 
                                    <div> 
                                      <a href="/AddUsers" onClick={this.handleClick2}>Add users to  Experiment</a>
                                  </div>  
                                  <div className="form-group">
                                        <a href="none"  onClick={this.viewdata}>Download Experiment Data as CSV file</a>
                                    </div> 
                         </form>
                </div>
                </div>
         </div>
     );
   }
 }