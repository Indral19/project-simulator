import React from "react";
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import moment from 'moment';
import http from "../http-common";
import Papa from 'papaparse';
export default class createexperiment extends React.Component {
   
    constructor() 
    {
     super();
     this.state= {
        expname :'',
        expdt : '',
        csvFile: File,
        jsFile:FileList  
      };
      this.prepareData = this.prepareData.bind(this);
    } 

    componentDidMount() {
        if(((sessionStorage.getItem('userid'))==null)||((sessionStorage.getItem('acctype'))!=2))
        {
           toast.warning('Unauthorized');
           sessionStorage.clear();
           this.props.history.push({pathname:'/Home'});
        }
    
    }


     onFileChange = event =>
        {
            this.setState({ csvFile: event.target.files[0] });
        };
    
    onJSFileChange=event =>
        {
            this.setState({ jsFile: event.target.files });
        };

    resetCreateExpt(e)
        {
            this.setState({expname:'' }); 
            this.setState({ csvFile:'' }); 
            this.setState({ jsFile:'' }); 
            this.setState({ expdt:'' }); 
        }
    
        importCSV = () => {
            
            const { csvfile } = this.state;
            Papa.parse(this.state.csvFile, {
              complete: this.prepareData,
              header: false
            });
          };
        
          prepareData(result) { 
            //e.preventDefault();  
            var data1 = result.data;
            this.setState({data : data1});
              
    
              var c=JSON.parse(JSON.stringify(this.state.data));
              
              var dparams=[];
              var iparams=[];
              var lparams=[];
              c.forEach((element, index, array) => {
                 dparams.push(array[index][0]);
               });
              dparams.shift();
              dparams=dparams.join(" ").trim().split(' ');
              c.forEach((element, index, array) => {
                iparams.push(array[index][1]);
              });
              iparams.shift();
              iparams=iparams.join(" ").trim().split(' ');
              c.forEach((element, index, array) => {
                 lparams.push(array[index][2]);
              });
              lparams.shift();
              lparams=lparams.join(" ").trim().split(' ');
              var nparams=dparams.length+iparams.length+lparams.length;
              var d =JSON.stringify(dparams);
              dparams=d.replace("[","{");
              dparams=dparams.replace("]","}");
              var i=JSON.stringify(iparams);
              iparams=i.replace("[","{");
              iparams=iparams.replace("]","}");
              var l=JSON.stringify(lparams);
              lparams=l.replace("[","{");
              lparams=lparams.replace("]","}");     
              var vno =1;
              this.setState({nps : nparams});
              const formData = new FormData();
           
            formData.append("expname",this.state.expname);
            formData.append("exadminid",sessionStorage.getItem('userid'));
            var i;
            var m=this.state.jsFile.length
            for(i=0;i<=m-1;i++)
            {
              formData.append("jsfile",this.state.jsFile[i],this.state.jsFile[i].name);
            }
         
            formData.append("vno",JSON.stringify(vno));
            formData.append("nps",JSON.stringify(nparams));
            formData.append("dparams",JSON.stringify(dparams));
            formData.append("iparams",JSON.stringify(iparams));
            formData.append("lparams",JSON.stringify(lparams));
                 
            
          if(moment().isBefore(this.state.expdt))
           {
            formData.append("expdt",this.state.expdt);
                http.post('/createExpt', formData)
                .then(res =>
                 {
                    if(res.status===200)
                    {
                        toast.success('experiment created');
                        this.props.history.push({pathname:'/Eadmin'});
        }
                    else
                    {
                        toast.error('error');
                    }
                }
                );
          }
          else
          {   toast.error('Expiry date should be greater than current date');
              this.props.history.push({pathname:'/Eadmin'});
          }
          }// preparedata 

    onFileUpload = (e) => 
        {
            e.preventDefault();
            this.importCSV();
            
        };
   

    render() {
        return(
        <div>
            <div class="card login">
                <div class="card-body login-card-body">
             <form>   
              <h4 class ="text-center">Create Experiment </h4> 
                <div className="form-group">
                    <label class ="text-center" >Experiment name</label>
                    <input type="text"  className="form-control" placeholder="Experiment name" onChange={(e) => {this.setState({ expname: e.target.value}) } }/>
                <div >
                <br></br>
                <div className="form-group">
                    <label>Experiment Variables from CSV file </label>
                    <label>Upload</label>
                    <input type="file" name="csvfile" accept=".csv" onChange={this.onFileChange} />
                </div>
                <br></br>
                <div className="form-group">
                    <label>Select the JS file to be used for experiment </label>
                    <label>Upload</label>
                    <input type="file"  name="jsfile" accept=".js" enctype="multipart/form-data" onChange={this.onJSFileChange} multiple />
                </div> 
                <br></br>
                <div className="form-group">
                  <label>Experiment Expiry date</label>
                  <input type="date" className="form-control"  placeholder="Select date" name="dates" onChange={(e) => {this.setState({expdt : e.target.value})}}/>
                </div>
                <br></br>  
                </div>
                <div>
                <button type="submit" className="btn btn-primary mr-1" disabled={!this.state.expname || !this.state.jsFile || !this.state.csvFile || !this.state.expdt} onClick={(e) => this.onFileUpload(e)} >Create</button>
                <span>            </span>
                <button  type="reset" className="btn btn-secondary" onClick={(e) => this.resetCreateExpt(e)} >Reset</button>
                </div> 
                </div>
                
                <div>
                 <br></br> 
                 {/* when build is done change this to actual path of app http addr downloads from public folder of sim-web-app1 folder
                 */}
                 <a href='./dataCustomer.csv' download> HELP :Download CSV template </a>
                 <br></br> 
                 <a href='./exp_simulation_start.txt' download> HELP :Download Simulation start Code Template </a>
                 <br></br> 
                 <a href='./exp_simulation_termination.txt' download> HELP :Download Simulation termination Code Template </a>
                </div>
              </form>  
              </div>
              </div>
        </div>
        )
    }
}