import React, { Component } from "react";
import http from "../http-common";
import { toast } from 'react-toastify';
import * as FileSaver from 'file-saver';

export default class ViewData extends Component {
   constructor() {
      super();
      this.state = {
         expnam :'',
         expid :0,
         adminid :0,
         verid : 0,
         uid :0,
         exptmnt :[],
         exptmnt1 :[],
         datas :[],
         datas1 :[],
         sessionid:'',
         filename:'',
         resultnull : false,
       }
   }

   componentDidMount() {
   
       this.state.adminid = sessionStorage.getItem('userid');
       this.setState({expid : sessionStorage.getItem('expid')});
       this.setState({expnam: sessionStorage.getItem('expnam')});
       if(((sessionStorage.getItem('userid'))==null)||((sessionStorage.getItem('acctype'))!=2))
               {
                  toast.warning('Unauthorized');
                  sessionStorage.clear();
                  this.props.history.push({pathname:'/Home'});
               }
      else 
            {
              http.get('/eadminexpusrssn/' +sessionStorage.getItem('expid'))
                .then(res => 
                  {
                            if(res.status===500)
                            {
                              toast.error('error');
                            }
                            else
                            {
                            this.setState({ exptmnt: res.data });
                            if(res.data == "") {
                              this.setState({resultnull : true});        
                            } 
                            else {
                            this.state.sessionid = this.state.exptmnt[0]["sessionid"];
                            var eid =sessionStorage.getItem('expid');
                            fetch(process.env.REACT_APP_API_URL+'/getallParams', {
                             method: 'POST',
                               headers: {
                                   "Accept": "application/json",
                                   'Content-Type': 'application/json'
                               },
                               body: JSON.stringify({
                                  expid : eid
                               })
                           }).then ((resp) => {
                                  if (resp.status === 200) {
                                      resp.json().then((result) => {
                                         this.setState({datas:result});
                                      }
                                      )}
                                 else {
                                       //resultnull is true
                                      }
                           }) ; 
                          }
                           }
                  }
                )
      }
   }

   simdatdwnld(id, e)
   {
    e.preventDefault();
    var fname= "simapp_"+id+".csv";
    var simapd = id;
    fetch(process.env.REACT_APP_API_URL+'/getsimdata', {
     method: 'POST',
       headers: {
           "Accept": "application/json",
           'Content-Type': 'application/json'
       },
       body: JSON.stringify({
          sid : simapd
         })
   }).then ((resp) => {
          if (resp.status === 200) {
               resp.json().then ((result) => {
                 this.setState({datas1:result});
                 this.state.exptmnt1[0] = "Realtime" + ',';
                 var dlen = this.state.datas[0].doubleparams.length;
                 var flag;
                 if ( dlen >0){
                  for(var i=0; i<dlen; i++) {
                    if(this.state.datas[0].doubleparams == '') {
                      flag =true; 
                      break;}
                    if( i< (dlen -1) ) this.state.exptmnt1[0] +=  this.state.datas[0].doubleparams[i]+',';
                    else this.state.exptmnt1[0] +=  this.state.datas[0].doubleparams[i];
                   }
                }
                
                var ilen = this.state.datas[0].intparams.length;
                if (ilen > 0){
                  if (!flag) this.state.exptmnt1[0] +=','; 
                  flag = false;
                  for(var i=0; i<ilen; i++) { 
                   if(this.state.datas[0].intparams == '') {
                    flag =true; 
                    break;}
                   if( i< (ilen -1) ) this.state.exptmnt1[0] +=  this.state.datas[0].intparams[i]+',';
                   else this.state.exptmnt1[0] +=  this.state.datas[0].intparams[i];
                  }
                }
                
                var llen = this.state.datas[0].links.length;
                if (ilen > 0){
                 if (!flag) this.state.exptmnt1[0] +=','; 
                 for(var i=0; i<llen; i++) {
                  if( i< (llen -1) ) this.state.exptmnt1[0] +=  this.state.datas[0].links[i]+',';
                  else this.state.exptmnt1[0] +=  this.state.datas[0].links[i];
                 }
                } 
   
                var rowz= this.state.datas1.length;
                 var j1=1;
                 this.state.exptmnt[0] = this.state.exptmnt1[0];
                 for(var j=0; j<rowz; j++) {
                  this.state.exptmnt[j1] = "\n"+this.state.datas1[j].realtime;
                  //all double values          
                  var dlen= this.state.datas1[j].dblval.length;
                  if (dlen > 0) { this.state.exptmnt[j1] += ',';
                  for(var k=0; k<dlen; k++){
                   if (k <(dlen-1)) this.state.exptmnt[j1] += this.state.datas1[j].dblval[k]+",";
                   else this.state.exptmnt[j1] += this.state.datas1[j].dblval[k];
                  }}
                  //all int values
                  var ilen= this.state.datas1[j].intval.length;
                  if(ilen > 0 ) {this.state.exptmnt[j1] += ',';
                  for(var k=0; k<ilen; k++){
                   if (k <(ilen-1)) this.state.exptmnt[j1] += this.state.datas1[j].intval[k]+",";
                   else this.state.exptmnt[j1] += this.state.datas1[j].intval[k];
                  }}
                  //all link values 
                  var llen= this.state.datas1[j].links.length;
                  if(llen > 0) {this.state.exptmnt[j1] += ',';
                  for(var k=0; k<llen; k++){
                   if (k <(llen-1)) this.state.exptmnt[j1] += this.state.datas1[j].links[k]+",";
                   else this.state.exptmnt[j1] += this.state.datas1[j].links[k];
                  }}   
                  j1+=1;
                  }
                  let blob = new Blob([this.state.exptmnt], { type: 'text/csv;charset=utf-8' });
                  FileSaver.saveAs(blob, fname);
                  this.props.history.push({pathname:'/ManageExpt'});
               }); 
           }
         else {
                //Err getting data from server..
              }
   }) ; 
  
  }  
  
     render() {
       if(!this.state.resultnull) {
        return (
           <div>
         <div class="row">
         <div class="col-10">
           <div class="card">
             <div class="card-header">
               <h3 class="text-center"><p className ="expstat">Experiment name : {this. state.expnam}  </p></h3>
     
               <div class="card-tools">
                 <div class="input-group input-group-sm" >
                   <input type="text" name="table_search" class="form-control float-right" placeholder="Search"/>
     
                   <div class="input-group-append">
                     <button type="submit" class="btn btn-default">
                       <i class="fas fa-search"></i>
                     </button>
                   </div>
                 </div>
               </div>
             </div>
              <div class="card-body table-responsive p-0">
               <table class="table table-hover text-nowrap">
                 <thead class="header">
                   <tr>
                     <th>ExperimentID</th>  
                     <th>UserID</th>
                     <th>SessionID</th>
                     <th>Download Data</th>
                   </tr>
                 </thead>
                 <tbody>
                 {this.state.exptmnt.map(exptmnt =>
                   <tr>
                     <td>{exptmnt.experimentid}</td>
                     <td>{exptmnt.userid}</td>  
                     <td>{exptmnt.sessionid}</td>
                       <td><button type="submit" className="btn  btn-primary " onClick={(e) => this.simdatdwnld(exptmnt.sessionid,e)} >Download data</button></td> 
                   </tr> 
                 )}
                 </tbody>
                 </table>
                 </div>
                 </div>
                 </div>
                 </div>
                 </div>
             );
           } 
           else {
              return (
               <div class="row">
               <div class="col-10">
                 <div class="card">
                   <div class="card-header">
                     <h3 class="text-center"><p className ="expstat">Experiment name : {this.state.expnam}   </p></h3>
                     <div class="card-tools">
                       <div class="input-group input-group-sm" >
                         <input type="text" name="table_search" class="form-control float-right" placeholder="Search"/>
           
                         <div class="input-group-append">
                           <button type="submit" class="btn btn-default">
                             <i class="fas fa-search"></i>
                           </button>
                         </div>
                       </div>
                     </div>
                   </div>
                  
                    <div class="card-body table-responsive p-0">
                     <table class="table table-hover text-nowrap">
                       <thead class="header">
                         <tr>
                         <th>ExperimentID</th>  
                         <th>UserID</th>
                         <th>SessionID</th>
                         <th>Download Data</th>
                         </tr>
                       </thead>
                       <tbody>
                       </tbody>
                       </table>
                       <div className="form-group">
                         <h6><p className="expstat1">- - - - - - - - - - - - - -  N  o    S  e  s  s  i  o  n  s - - - - - - - - - - - - - - </p></h6> 
                        </div>    
                       </div>
                       </div>
                    </div>
                    </div>
                 );
            }
        
           }

}   