import React, { Component } from "react";
import http from "../http-common";
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
  
//  import './Terms.css'
toast.configure() 
export default class Myprofile extends Component {
    constructor(props) {
        super(props);
        this.state = {
          users: {},
          profile:Boolean,
          msg:'',
          uname:'',
          emailid:'',
          gender:'',
          agegroup:'',
          industry:'',
          highestdegree:'',
          profile_type:'',
          id:'',
          
          proid:'',
          isnew:Boolean
          
        };
      }
    componentDidMount() 
    {
        if(((sessionStorage.getItem('userid'))==null)||((sessionStorage.getItem('acctype'))!=3))
        {
           toast.warning('Unauthorized');
           sessionStorage.clear();
           this.props.history.push({pathname:'/Home'});
        }
         else if((sessionStorage.getItem('userid'))!=null)
           {
               http.get('/usersbyid/'+sessionStorage.getItem('userid'))
               .then(res => 
                {
                           if(res.status===500)
                                {
                                    toast.error('error');
                                }
                                else
                                {
                                this.setState({ users: res.data });
                                this.setState({uname:this.state.users[0].uname})
                                this.setState({emailid:this.state.users[0].emailid})
                                this.setState({proid:this.state.users[0].profileid})
                                http.get('/profilebyid/'+this.state.proid)
                                .then(res => 
                                        {       
                                            if(res.status===500)
                                                {
                                                    toast.error('error');
                                                }
                                            else
                                            {
                                                if((res.data.length)===0)
                                                {
                                                this.setState({profile:true}) 
                                                }
                                                else 
                                                {
                                                this.setState({profile:false})
                                                }
                                            }    
                                        }
                                    )
                                }
                            }
                            )
                        
                        }
    }
    
    Update(e)
    {
       e.preventDefault();
                        const { gender,agegroup,industry,highestdegree,profile_type } = this.state;
                        http.post('/updateprofile/'+this.state.proid,{gender,agegroup,industry,highestdegree,profile_type})
                        .then
                          (res =>
                            {
                                if(res.status===500)
                                {
                                    toast.error('error');
                                }
                                else
                                {
                                toast.success('success');
                                this.props.history.push({pathname:'/MyExper'});
                                }
                            }
                          )
    }

    Add(e)
    {
        e.preventDefault();
        const { gender,agegroup,industry,highestdegree,profile_type } = this.state;
        http.post('/profileuser/'+sessionStorage.getItem('userid'),{gender,agegroup,industry,highestdegree,profile_type})
          .then (res => {
             if(res.status===500)
               {
                  toast.error('error');
               }
             else
              {
                  toast.success('success');
                  this.props.history.push({pathname:'/MyExper'});
              }
        })
    }
  

    render()
    {
        return(
            <div>
                 <div class="card login">
                        <div class="card-body login-card-body">
                                <h4 class="text-center">My profile </h4>
                                <br></br>
                                        <form >
                                            <div class="form-group">
                                                <label class="label" for="uname">User Name:</label>
                                                <input type="text" class="form-control" name="uname" value={this.state.uname}   />
                                            </div>
                                            <br></br>
                                            <div class="form-group">
                                                <label class="label" for="uemail">User Email:</label>
                                                <input type="text" class="form-control" name="emailid" value={this.state.emailid}   />
                                            </div>
                                            <br></br>
                                            <div class="form-group">
                                                <label class="label" for="uemail">Gender:</label>
                                                <select  class="form-control" name="gender" onChange={(e) => {this.setState({gender : e.target.value}) }} >
                                                <option value="">select</option>
                                                <option value="Male">Male</option>
                                                <option value="Female">Female</option>
                                                <option value="Others">Others</option>
                                                </select>
                                            </div>
                                            <br></br>
                                            <div class="form-group">
                                                <label class="label" for="uemail">Age group:</label>
                                                <select  class="form-control" name="agegroup" onChange={(e) => {this.setState({agegroup : e.target.value}) }} >
                                                <option value="">select</option>
                                                <option value="1">11-29</option>
                                                <option value="2">30-39</option>
                                                <option value="3">40-49</option>
                                                <option value="4">50-59</option>
                                                <option value="5">60 & Above</option>
                                                </select>
                                           </div> 
                                           <br></br>
                                            <div class="form-group">
                                                <label class="label" for="uemail">Profession:</label>
                                                <select  class="form-control" name="profile_type" onChange={(e) => {this.setState({profile_type : e.target.value}) }} >
                                                <option value="">select</option>
                                                <option value="Student">Student</option>
                                                <option value="Faculty">Faculty</option>
                                                <option value="Employeed">Employeed</option>
                                                <option value="Self-Employeed">Self-Employeed</option>
                                                <option value="Others">Others</option>
                                                </select>
                                            </div> 
                                            <br></br>
                                            <div class="form-group">
                                                <label class="label" for="uemail">Industry:</label>
                                                <input type="text" class="form-control" name="industry" onChange={(e) => {this.setState({industry : e.target.value}) }}   />
                                            </div>
                                            <br></br>
                                           <div class="form-group">
                                                <label class="label" for="uemail"> Degree:</label>
                                                <select  class="form-control" name="highestdegree" onChange={(e) => {this.setState({ highestdegree: e.target.value}) }} >
                                                <option value="">select</option>
                                                <option value="1">High School</option>
                                                <option value="2">Diploma</option>
                                                <option value="3">Bachelors</option>
                                                <option value="4">Masters</option>
                                                <option value="5">Doctorates</option>
                                                <option value="6">Others</option>
                                                </select>
                                           </div>
                                           <br></br>
                                           <div class="form-group">
                                            { 
                                            this.state.profile ? 
                                              (
                                            <button type="submit" className="btn btn-primary btn-block "disabled={!this.state.gender || !this.state.agegroup || !this.state.profile_type || !this.state.industry ||!this.state.highestdegree} onClick={(e) => this.Add(e) } >Submit</button>
                                              ):
                                              (
                                            <button type="submit" className="btn btn-primary btn-block " disabled={!this.state.gender || !this.state.agegroup || !this.state.profile_type || !this.state.industry ||!this.state.highestdegree} onClick={(e) => this.Update(e) } >Update</button>
                                              )
                                            }
                                           </div>
                                        </form>
                        </div>   
                    </div>
            </div>
               
        )
        
    }
}