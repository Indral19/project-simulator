import React, { Component } from "react";
import http from "../http-common";
import  PHeader from '../Shared/PHeader';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css'; 



export default class Mytask extends Component {
    constructor(props) {
        super(props);
        this.state = {
            users:{},
            ename:'',
            expid:'',
            no:'',
            ssn:{}
        }
    }

    componentDidMount() {
        localStorage.setItem('compltd',false);
        if(((sessionStorage.getItem('userid'))==null)||((sessionStorage.getItem('acctype'))!=3))
        {
                  toast.warning('Unauthorized');
                  sessionStorage.clear();
                  this.props.history.push({pathname:'/Home'});
        }
        else if((sessionStorage.getItem('userid'))!=null)
        {
        http.get('/JSFile/'+this.props.match.params.id)
        .then(res => {
            if(res.status===500)
            {
                toast.error('error');
            }
           else
            {
                this.setState({ users: res.data });
                this.setState({no:this.state.users.length})
                this.setState({ename:this.state.users[0].experimentname})
                this.setState({expid:this.state.users[0].experimentid})
           }
         }
        )
     }
    }


    ready(id){
        localStorage.setItem('apiurl',process.env.REACT_APP_API_URL );
        localStorage.setItem('clienturl',process.env.REACT_APP_CLIENT_URL );
        localStorage.setItem('uid',sessionStorage.getItem('userid'));
        localStorage.setItem('eid',sessionStorage.getItem('expid'));
        window.open(localStorage.getItem('clienturl')+ '/src/'+id);
        this.props.history.push({pathname:'/MyExper'});
        this.props.history.push({pathname:'/Logout'});
        
       }


   render(){
        return(
            <div>
                <div>
                <PHeader></PHeader>
                </div>
            <div class="card login">
                <div class="card-body login-card-body">
                <h3 class="text-center">My Task</h3>
                <br></br>
                <div class="myta"  >
                    Dear <strong> {sessionStorage.getItem('username')} </strong>
                    <br></br>
                     <p>Thank you for participating in <strong>{this.state.ename}</strong></p>
                     <p>please click the link below to start your experiment  </p>
                       <p>  You will be first shown the instruction.After reading them, you may start the experiment.</p>
                       <p>please contact <u>Prof. Niket Kaisare</u> or <u>Prof. Rajagopal Srinivasan</u> if you have any queries or suggestions</p>
                       <br></br>
                       <br></br>
                       <div class="form-group">
                <button type="submit" className="btn btn-primary btn-block " onClick={(e) => this.ready(this.state.expid) } >I AM READY</button>
                {/* <div class="no data">
                <p> you are assigned for any Experiment</p>
                </div> */}
                </div>
                </div>
                </div>
                </div>
            </div>
        )
    }

}