import React, { Component } from "react";

import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import http from "../http-common";
export default class Terms extends Component {
    constructor(props) {  
        super(props);  
        this.state = {  
           terms:''
          
        };  
    }

    componentDidMount() 
    {
      if(((sessionStorage.getItem('userid'))==null)||((sessionStorage.getItem('acctype'))!=3))
               {
                  toast.warning('Unauthorized');
                  sessionStorage.clear();
                  this.props.history.push({pathname:'/Home'});
               }
    }
  
    Agree(id,e)
    {
      e.preventDefault();     
      http.put('/termsagreed/'+id)
      .then(res =>  
      {
        if(res.status===500)
            {
              toast.error('error');
            }
        else
        {    
          this.props.history.push({pathname:'/Myprofile'});
          // window.location.href=process.env.REACT_APP_CLIENT_URL+'/Myprofile'
        }  
      }
      );
    }
  
    handleCheckboxChange(e){
        this.setState({terms : e.target.value})
    }
  
    cancel()
    {
        window.location.reload()
    }
  
    render()
    {
        return (
            <div>
                 <div class="card login">
                 <div class="card-body login-card-body">
                 <form >
                 <div>    
                 <div>
                 <h3 class="text-center">Terms and Condition</h3>
                 <div class="terms">
                 <p>Please read these Terms carefully. Access to, and use of  products (“Products”),  services (“Services”), and the  website https://app..com/ (“Website”), including any of its content, is conditional on your agreement to these Terms. You must read, agree with, and accept all of the terms and conditions contained in these Terms. By creating an account, or by using or visiting our Website, you are bound to these Terms and you indicate your continued acceptance of these Terms.</p>
                 <input type="checkbox"  name="terms" onChange={this.handleCheckboxChange.bind(this)} />
                 <label>I have read the terms and condition</label>
                 {
                   (this.state.terms) ?
                   (
                       <div class="su">
                         <button class="btn btn-primary mr-1" type="submit" onClick={(e) => this.Agree(sessionStorage.getItem('userid'),e)}>I Agree</button>
                          <button class="btn btn-primary " onClick={() => this.cancel() } type="submit">Cancel</button>
                       </div>
                   ):(
                        <p></p>
                     )
                 }
                 </div>
                 </div>
                 </div>
                 </form>
                 </div>
                 </div>
          </div>
          )
    }
}