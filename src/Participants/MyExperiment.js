import React, { Component } from "react";
import  PHeader from '../Shared/PHeader';
import { toast } from 'react-toastify';
import http from "../http-common";


import 'react-toastify/dist/ReactToastify.css';
export default class Myprofile extends Component {
  dflag = true;
    constructor(props) 
    {
              super(props);
              this.state = 
              {
                 users:[],
                  no:'',
                 prevusers:[]
              }

    }
    
    componentDidMount() 
    {
      if(((sessionStorage.getItem('userid'))==null)||((sessionStorage.getItem('acctype'))!=3))
      {
                toast.warning('Unauthorized');
                sessionStorage.clear();
                this.props.history.push({pathname:'/Home'});
              }
              else if((sessionStorage.getItem('userid'))!==null)
              {
                this.updatestate();
              }
           }

 
  dateprinter=(date)=>
    {
              const options = {  year: 'numeric', month: 'long', day: 'numeric' };
              return (new Date(date)).toLocaleString(undefined,options)
    }
 
 
  ready(id)
    {
              sessionStorage.setItem('expid',id);
              sessionStorage.setItem('cflag',false);
              this.props.history.push({pathname:'/Mytask/'+id});
              
             
    }

  
    updatestate()
    {
      http.get('/experimentsbyuid/'+sessionStorage.getItem('userid'))
      .then(res => 
                    {
                      if(res.status===500)
                        {
                          toast.error('error');
                        }
                      else
                        {
                          this.setState({ users: res.data });
                          this.setState({no:this.state.users.length})
                        }
                    }
            ); 
    }
    


    render()
    {
             const number =this.state.no;
              if (number!==0) 
              {
             
              } 
              else 
              {
              // toast.error("No Experiment");
              }
              
      return(

            <div>
                 <PHeader></PHeader>
                          <div class="card Experiment">
                              <div class="card-body Experi-card-body">     
                                <div class="card-header">
                                  <h4 class="text-center">My Experiments</h4>
                                    <div class="card-body table-responsive p-0">
                                      {/* <div> {comp}</div>  */}
                                        <table class="table table-hover text-nowrap">
                                            <thead class="header">
                                              <tr>
                                                  <th>ID</th>
                                                  <th>Experiments</th>
                                                  <th>Invite Date</th>
                                                  <th>CompletedTimes</th>
                                                  <th>Run</th>
                                              </tr>
                                            </thead>
                                            <tbody>
                                            {
                                              this.state.users.map(users =>
                                                <tr>
                                                    <td>{users.experimentid}</td>
                                                    <td>{users.experimentname}</td>
                                                    <td>{this.dateprinter(users.srvnvitedt)}</td>
                                                    <td>{users.completedtimes}</td>
                                                    {users.allowedtimes === users.completedtimes ? <td><i  ></i></td> : <td><i class="fas fa-play" onClick={(e) => this.ready(users.experimentid) } ></i></td>}
                                                   
                                                    {/* <td><i class="fas fa-play" onClick={(e) => this.ready(users.experimentid) } ></i></td> */}
                                                </tr>
                                              )
                                            }
                                            </tbody>
                                          </table>
                                     </div>
                                  </div>
                                </div>
                            </div>
                         </div>
            
        )
  }
}