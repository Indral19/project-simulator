import React, { Component } from "react";
import http from "../http-common";

import { toast } from "react-toastify";
 export default class AdduserfromCSV extends Component {
   
   state = {
      selectedFile: null
      }; 

componentDidMount() 
    {
      if(((sessionStorage.getItem('userid'))==null)||((sessionStorage.getItem('acctype'))!=1))
      {
         toast.warning('Unauthorized');
         sessionStorage.clear();
         this.props.history.push({pathname:'/Home'});
      }
    }

   onFileChange = event => {
      this.setState({ selectedFile: event.target.files[0] });
     };
   
   onFileUpload = () => {
      const formData = new FormData();
      formData.append("csvfile",this.state.selectedFile,this.state.selectedFile.name);
      http.post('/uploadCSV',formData)
      .then(res =>
        {
          if(res.status==203)
          {
         toast.error(res.data)
          }
        else
         {
          toast.success(res.data)
          this.props.history.push({pathname:'/ManageUsers'});
        }
       }
       );
    };
    

  render() 
   {
      return (
        <div class="card login">
        <div class="card-body login-card-body">
        <div class="container">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="text-center">
                Add User
              </h4>
              <br></br>
            </div>
            <div >
            <input type="file" name="csvfile" onChange={this.onFileChange} />
            <button class="btn btn-primary mr-1"  disabled={!this.state.selectedFile} onClick={this.onFileUpload}>
             Upload
            </button>
              </div>
            </div>
            </div>
            </div>
            </div>
            );
      }
 }
