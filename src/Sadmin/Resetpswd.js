import React, { Component } from "react";
import { toast } from 'react-toastify';
import http from "../http-common";
export default class About extends Component {
          constructor(props) 
          {
            super(props);
            this.state = 
            {
              emailid :'',
              expire:Boolean,
              paswd:'',
              conwd:''  
            }
          }
componentDidMount ()
   {
      var t= new URLSearchParams(this.props.location.search).get("xwer");
      var t1=new URLSearchParams(this.props.location.search).get("gter");
      var t2=new URLSearchParams(this.props.location.search).get("vfge");
          if(t==null||t1==null||t2==null) 
        {
            this.setState({expire:false})
            toast.warning('Unauthorized')
            this.props.history.push({pathname:'/Home'});
        }
      else if(Date.now()<t2)
          {
            this.setState({expire:true})
            this.setState({emailid:t})
            
          }
        else 
        {
          this.props.history.push({pathname:'/Home'});
          toast.warning('Link Expired') 
        }
}  
   
validate(event) 
{
     var pass = event.target.value;
     const re = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})");
     const isOk = re.test(pass);
     if (isOk) 
     {
       document.getElementById("pwd").innerHTML="strong";
       document.getElementById("pwd").style.color = "green"
     } 
     else
     {
       document.getElementById("pwd").innerHTML="Weak"  
       document.getElementById("pwd").style.color = "red"
     }
}


validate1(event) 
{
  var pass = event.target.value;
  const re = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})");
  const isOk = re.test(pass);
  if (isOk) 
  {
    document.getElementById("pwd1").innerHTML="strong";
    document.getElementById("pwd1").style.color = "green"
  } 
  else
  {
    document.getElementById("pwd1").innerHTML="Weak"  
    document.getElementById("pwd1").style.color = "red"
  }
}

changepwd(e)
 {
     e.preventDefault();
     const { emailid,paswd,conwd } = this.state;
    if(this.state.paswd != this.state.conwd)  
    {   
        toast.error("Passwords did not match");  
    } 
    else 
    {  
        http.post('/resetpwd',{ emailid,paswd }) .then
        (res =>
        {
          if(res.status===500)
          {
            toast.error('Error')
          }
          else 
          {
            toast.success('password changed');
            this.props.history.push({pathname:'/Logout'});
          }
        }  
      )
    }  

 }
render()
{
  return(
         <div> 
            { 
              this.state.expire ? 
              (
               <div class="card login">
               <div class="card-body login-card-body">
                  <form >
                      <div>    
                        <div>
                        <h4 class="text-center">Reset Password</h4>
                        <div className="form-group">          
                                <div>
                                    <label class="label">Email</label>
                                    <input type="text" className="form-control"  placeholder="Enter Email" value={this.state.emailid}   />
                                    <p>
                                    <div id="email"></div>  
                                    </p>
                                </div>
                                <div>
                                  <label class="label">Password</label>
                                  <input type="password" className="form-control" placeholder="Enter password" onInput={this.validate  } onChange={(e) => {this.setState({paswd : e.target.value})}} />
                                  <p>
                                  <div id="pwd"></div>  
                                  </p>
                               </div>
                              
                                <div>
                                      <label class="label">Confirm Password</label>
                                      <input type="password" className="form-control" placeholder="Enter password" onInput={this.validate1  } onChange={(e) => {this.setState({conwd : e.target.value})}} />
                                      <p>
                                      <div id="pwd1"></div>  
                                      </p>
                                </div>  
                                <div>
                                    <button type="submit" className="btn btn-primary"  disabled={!this.state.emailid || !this.state.paswd||!this.state.conwd} onClick={(e) => this.changepwd(e)} >submit</button> 
                                </div>
                           </div> 
                      </div>
                    </div>
                </form>
            </div>
            </div>
             ):
              (
                <div></div>
              )                                    
            }
         </div>   
        )
    }
}