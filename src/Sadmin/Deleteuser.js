import React, { Component } from "react";
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import './Deleteuser.css';
import http from "../http-common";



export default class Deleteuser extends Component {
   constructor(props) {
      super(props);
      this.state = {
        users: [],
        acctype:'',
        msg:'',
        createddate:'',
        name:'',
        num:''
      };
    }
    
    componentDidMount() 
      {
      this.state.acctype=sessionStorage.getItem('acctype')
      const{acctype}=this.state
      if(((sessionStorage.getItem('userid'))==null)||((sessionStorage.getItem('acctype'))!=1))
         {
          toast.warning('Unauthorized');
          sessionStorage.clear();            
          this.props.history.push({pathname:'/Home'});
        }
      else 
       {
          http.post('/allusers',{acctype})
                .then(res => 
                  {
                            if(res.status==500)
                            {
                              toast.error('error');
                            }
                            else
                            {
                             console.log(res); 
                            this.setState({ users: res.data });
                            this.setState({ num: this.state.users.length });
                            }
                }
                )
            }
    }

    delete(id){
      http.put('/deleteusers/'+id)
        .then((res) => {
          if(res.status===500)
          {
            toast.error('error');
          }
          else{
             toast.success('user deleted',{autoClose:1000})
             this.props.history.push({pathname:'/Sadmin'});
          }
        }
        );
    }

    sort(){
       const{acctype}=this.state
       http.post('/sortusers',{acctype})
      .then(res => {
        if(res.status===500)
        {
          toast.error('error');
        }
        else
        {
        this.setState({ users: res.data });
        }
    }
    );
  }

   
  sortalpha()
  {
   const{acctype}=this.state
     http.post('/sortalp',{acctype})
      .then(res => 
      {
        if(res.status===500)
        {
          toast.error('error');
        }
        else
        {
        this.setState({ users: res.data });
        }
     }
    )
  }

    dateprinter=(date)=>{
      const options = {  year: 'numeric', month: 'long', day: 'numeric' };
       return (new Date(date)).toLocaleString(undefined,options)
    }

    timeprinter=(time)=>{
      // const options = {   hour: "2-digit", minute: "2-digit"  };
      return (time)
    }

    accounttype=(id)=>{
      if(id===1) return 'Super Admin'
      if(id===2) return 'Experimental Admin'
      if(id===3) return 'Participants'
      return ''
    }
    
render() 
{
  if((this.state.num)!==0){
    return(
      <div> 
            <div class="row">
              <div class="col-10">
                <div class="card">
                  <div class="card-header">
                    <h3 class="text-center">Users</h3>
                      <div class="card-tools">
                        <div class="input-group input-group-sm" >
                            <div class="input-group-append">
                                <button type="submit" class="btn btn-default">
                                  <i class="fas fa-arrow"></i>
                                </button>
                            </div>
                          </div>
                      </div>
                  </div>
          <div class="card-body table-responsive p-0">
            <table class="table table-hover text-nowrap">
              <thead class="header">
                <tr>
                  <th>ID</th>
                  <th>User<button class="btn btn-sm" onClick={() => this.sortalpha() }><i class="fas fa-sort-alpha-down"></i></button></th>
                  <th>Date<button class="btn btn-sm" onClick={() => this.sort() }><i class="fas fa-sort"></i></button></th>  
                  <th>Time</th>
                  <th>Account Type<button class="btn btn-sm" onClick={() => this.sortaccount() }></button></th>
                  <th>Email</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
              {this.state.users.map(users =>
                <tr>
                  <td>{users.userid}</td>
                  <td>{users.uname}</td>
                     <td>{this.dateprinter(users.createddate)}</td> 
                     <td>{this.timeprinter(users.createdtime)}</td>   
                  <td>{this.accounttype(users.account_typeid)}</td>
                  <td>{users.emailid}</td>
                  <td><button type="submit" className="btn  btn-primary " onClick={() => this.delete(users.userid) } >Delete</button></td>  
                </tr>
              )}
              </tbody>
            </table>
          </div>
         
        </div>
        
      </div>
    </div>
    
    {/* <div className="loginstat"  role="alert">{this.state.msg}</div>  */}
    </div>
   
    )
  }
  else{
    return(
      <div> 
    <div class="row">
      <div class="col-10">
        <div class="card">
          <div class="card-header">
            <h3 class="text-center">Users</h3>
           
            <div class="card-tools">
              <div class="input-group input-group-sm" >
                    <div class="input-group-append">
                      <button type="submit" class="btn btn-default">
                        <i class="fas fa-search"></i>
                      </button>
                    </div>
              </div>
            </div>
          </div>
          <div class="card-body table-responsive p-0">
          <table class="table table-hover text-nowrap">
            <tr>
            <td class="text-center">No Data</td>
            </tr>
            </table>
            </div>
        </div>
      </div>
    </div>
      </div>
    )
  }
  }
}